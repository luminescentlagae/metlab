package com.vinogratis.metlabmod.lib;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import java.util.List;

public class MetLabUtilsBlocks
{
    public static Block stone = Blocks.stone;
    public static Block grass = Blocks.grass;
    public static Block dirt = Blocks.dirt;
    public static Block cobblestone = Blocks.cobblestone;
    public static Block planks = Blocks.planks;
    public static Block sapling = Blocks.sapling;
    public static Block bedrock = Blocks.bedrock;
    public static Block waterMoving = Blocks.flowing_water;
    public static Block waterStill = Blocks.water;
    public static Block lavaMoving = Blocks.flowing_lava;

    /** Stationary lava source block */
    public static Block lavaStill = Blocks.lava;
    public static Block sand = Blocks.sand;
    public static Block gravel = Blocks.gravel;
    public static Block oreGold = Blocks.gold_ore;
    public static Block oreIron = Blocks.iron_ore;
    public static Block oreCoal = Blocks.coal_ore;
    public static Block wood = Blocks.log;
    public static Block leaves = Blocks.leaves;
    public static Block sponge = Blocks.sponge;
    public static Block glass = Blocks.grass;
    public static Block oreLapis = Blocks.lapis_ore;
    public static Block blockLapis = Blocks.lapis_block;
    public static Block dispenser = Blocks.dispenser;
    public static Block sandStone = Blocks.sandstone;
    public static Block music = Blocks.noteblock;
    public static Block bed = Blocks.bed;
    public static Block railPowered = Blocks.golden_rail;
    public static Block railDetector = Blocks.detector_rail;
    public static Block pistonStickyBase = Blocks.sticky_piston;
    public static Block web = Blocks.web;
    public static Block tallGrass = Blocks.tallgrass;
    public static Block deadBush = Blocks.deadbush;
    public static Block pistonBase = Blocks.piston;
    public static Block pistonExtension = Blocks.piston_head;
    public static Block cloth = Blocks.wool;
    public static Block pistonMoving = Blocks.piston_extension;
    public static Block plantYellow = Blocks.yellow_flower;
    public static Block plantRed = Blocks.red_flower;
    public static Block mushroomBrown = Blocks.brown_mushroom;
    public static Block mushroomRed = Blocks.red_mushroom;
    public static Block blockGold = Blocks.gold_block;
    public static Block blockIron = Blocks.iron_block;

    /** stoneDoubleSlab */
    public static Block stoneDoubleSlab = Blocks.double_stone_slab;

    /** stoneSingleSlab */
    public static Block stoneSingleSlab = Blocks.stone_slab;
    public static Block brick = Blocks.brick_block;
    public static Block tnt = Blocks.tnt;
    public static Block bookShelf = Blocks.bookshelf;
    public static Block cobblestoneMossy = Blocks.mossy_cobblestone;
    public static Block obsidian = Blocks.obsidian;
    public static Block torchWood = Blocks.torch;
    public static Block fire = Blocks.fire;
    public static Block mobSpawner = Blocks.mob_spawner;
    public static Block stairsWoodOak = Blocks.oak_stairs;
    public static Block chest = Blocks.chest;
    public static Block redstoneWire = Blocks.redstone_wire;
    public static Block oreDiamond = Blocks.diamond_ore;
    public static Block blockDiamond = Blocks.diamond_block;
    public static Block workbench = Blocks.crafting_table;
    public static Block crops = Blocks.wheat;
    public static Block furnaceIdle = Blocks.furnace;
    public static Block furnaceBurning = Blocks.lit_furnace;
    public static Block signPost = Blocks.standing_sign;
    public static Block doorWood = Blocks.wooden_door;
    public static Block ladder = Blocks.ladder;
    public static Block rail = Blocks.rail;
    public static Block stairsCobblestone = Blocks.stone_stairs;
    public static Block signWall = Blocks.wall_sign;
    public static Block lever = Blocks.lever;
    public static Block pressurePlateStone = Blocks.stone_pressure_plate;
    public static Block doorIron = Blocks.iron_door;
    public static Block pressurePlatePlanks = Blocks.wooden_pressure_plate;
    public static Block oreRedstone = Blocks.redstone_ore;
    public static Block oreRedstoneGlowing = Blocks.lit_redstone_ore;
    public static Block torchRedstoneIdle = Blocks.redstone_torch;
    public static Block torchRedstoneActive = Blocks.redstone_torch;
    public static Block stoneButton = Blocks.stone_button;
    public static Block snow = Blocks.snow_layer;
    public static Block ice = Blocks.ice;
    public static Block blockSnow = Blocks.snow;
    public static Block cactus = Blocks.cactus;
    public static Block blockClay = Blocks.clay;
    public static Block reed = Blocks.reeds;
    public static Block jukebox = Blocks.jukebox;
    public static Block fence = Blocks.fence;
    public static Block pumpkin = Blocks.pumpkin;
    public static Block netherrack = Blocks.netherrack;
    public static Block slowSand = Blocks.soul_sand;
    public static Block glowStone = Blocks.glowstone;

    /** The purple teleport blocks inside the obsidian circle */
    public static Block portal = Blocks.portal; //(BlockPortal)(new BlockPortal(90)).setHardness(-1.0F).setStepSound(soundGlassFootstep).setLightValue(0.75F).setUnlocalizedName("portal");
    public static Block pumpkinLantern = Blocks.pumpkin_stem; //(new BlockPumpkin(91, true)).setHardness(1.0F).setStepSound(soundWoodFootstep).setLightValue(1.0F).setUnlocalizedName("litpumpkin");
    public static Block cake = Blocks.cake; //(new BlockCake(92)).setHardness(0.5F).setStepSound(soundClothFootstep).setUnlocalizedName("cake").disableStats();
    public static Block redstoneRepeaterIdle = Blocks.unpowered_repeater; //(BlockRedstoneRepeater)(new BlockRedstoneRepeater(93, false)).setHardness(0.0F).setStepSound(soundWoodFootstep).setUnlocalizedName("diode").disableStats();
    public static Block redstoneRepeaterActive = Blocks.powered_repeater; //(BlockRedstoneRepeater)(new BlockRedstoneRepeater(94, true)).setHardness(0.0F).setLightValue(0.625F).setStepSound(soundWoodFootstep).setUnlocalizedName("diode").disableStats();

    /**
     * April fools secret locked chest, only spawns on new chunks on 1st April.
     */
    public static Block lockedChest = Blocks.trapped_chest; //(new BlockLockedChest(95)).setHardness(0.0F).setLightValue(1.0F).setStepSound(soundWoodFootstep).setUnlocalizedName("lockedchest").setTickRandomly(true);
    public static Block trapdoor = Blocks.trapdoor; //(new BlockTrapDoor(96, Material.wood)).setHardness(3.0F).setStepSound(soundWoodFootstep).setUnlocalizedName("trapdoor").disableStats();
    public static Block silverfish = Blocks.monster_egg; //(new BlockSilverfish(97)).setHardness(0.75F).setUnlocalizedName("monsterStoneEgg");
    public static Block stoneBrick = Blocks.stonebrick; //(new BlockStoneBrick(98)).setHardness(1.5F).setResistance(10.0F).setStepSound(soundStoneFootstep).setUnlocalizedName("stonebricksmooth");
    public static Block mushroomCapBrown = Blocks.brown_mushroom; //(new BlockMushroomCap(99, Material.wood, 0)).setHardness(0.2F).setStepSound(soundWoodFootstep).setUnlocalizedName("mushroom");
    public static Block mushroomCapRed = Blocks.red_mushroom; //(new BlockMushroomCap(100, Material.wood, 1)).setHardness(0.2F).setStepSound(soundWoodFootstep).setUnlocalizedName("mushroom");
    public static Block fenceIron = Blocks.iron_bars; //(new BlockPane(101, "fenceIron", "fenceIron", Material.iron, true)).setHardness(5.0F).setResistance(10.0F).setStepSound(soundMetalFootstep).setUnlocalizedName("fenceIron");
    public static Block thinGlass = Blocks.glass_pane; //(new BlockPane(102, "glass", "thinglass_top", Material.glass, false)).setHardness(0.3F).setStepSound(soundGlassFootstep).setUnlocalizedName("thinGlass");
    public static Block melon = Blocks.melon_block; //(new BlockMelon(103)).setHardness(1.0F).setStepSound(soundWoodFootstep).setUnlocalizedName("melon");
    public static Block pumpkinStem = Blocks.pumpkin_stem; //(new BlockStem(104, pumpkin)).setHardness(0.0F).setStepSound(soundWoodFootstep).setUnlocalizedName("pumpkinStem");
    public static Block melonStem = Blocks.melon_stem; //(new BlockStem(105, melon)).setHardness(0.0F).setStepSound(soundWoodFootstep).setUnlocalizedName("pumpkinStem");
    public static Block vine = Blocks.vine; //(new BlockVine(106)).setHardness(0.2F).setStepSound(soundGrassFootstep).setUnlocalizedName("vine");
    public static Block fenceGate = Blocks.fence_gate; //(new BlockFenceGate(107)).setHardness(2.0F).setResistance(5.0F).setStepSound(soundWoodFootstep).setUnlocalizedName("fenceGate");
    public static Block stairsBrick = Blocks.brick_stairs; //(new BlockStairs(108, brick, 0)).setUnlocalizedName("stairsBrick");
    public static Block stairsStoneBrick = Blocks.stone_brick_stairs; //(new BlockStairs(109, stoneBrick, 0)).setUnlocalizedName("stairsStoneBrickSmooth");
    public static Block mycelium = Blocks.mycelium; //(BlockMycelium)(new BlockMycelium(110)).setHardness(0.6F).setStepSound(soundGrassFootstep).setUnlocalizedName("mycel");
    public static Block waterlily = Blocks.waterlily; //(new BlockLilyPad(111)).setHardness(0.0F).setStepSound(soundGrassFootstep).setUnlocalizedName("waterlily");
    public static Block netherBrick = Blocks.nether_brick; //(new Block(112, Material.rock)).setHardness(2.0F).setResistance(10.0F).setStepSound(soundStoneFootstep).setUnlocalizedName("netherBrick").setCreativeTab(CreativeTabs.tabBlock);
    public static Block netherFence = Blocks.nether_brick_fence; //(new BlockFence(113, "netherBrick", Material.rock)).setHardness(2.0F).setResistance(10.0F).setStepSound(soundStoneFootstep).setUnlocalizedName("netherFence");
    public static Block stairsNetherBrick = Blocks.nether_brick_stairs; //(new BlockStairs(114, netherBrick, 0)).setUnlocalizedName("stairsNetherBrick");
    public static Block netherStalk = Blocks.nether_wart; //(new BlockNetherStalk(115)).setUnlocalizedName("netherStalk");
    public static Block enchantmentTable = Blocks.enchanting_table; //(new BlockEnchantmentTable(116)).setHardness(5.0F).setResistance(2000.0F).setUnlocalizedName("enchantmentTable");
    public static Block brewingStand = Blocks.brewing_stand; //(new BlockBrewingStand(117)).setHardness(0.5F).setLightValue(0.125F).setUnlocalizedName("brewingStand");
    public static Block cauldron = Blocks.cauldron; //(BlockCauldron)(new BlockCauldron(118)).setHardness(2.0F).setUnlocalizedName("cauldron");
    public static Block endPortal = Blocks.end_portal; //(new BlockEndPortal(119, Material.portal)).setHardness(-1.0F).setResistance(6000000.0F);
    public static Block endPortalFrame = Blocks.end_portal_frame; //(new BlockEndPortalFrame(120)).setStepSound(soundGlassFootstep).setLightValue(0.125F).setHardness(-1.0F).setUnlocalizedName("endPortalFrame").setResistance(6000000.0F).setCreativeTab(CreativeTabs.tabDecorations);

    /** The rock found in The End. */
    public static Block whiteStone = Blocks.end_stone; //(new Block(121, Material.rock)).setHardness(3.0F).setResistance(15.0F).setStepSound(soundStoneFootstep).setUnlocalizedName("whiteStone").setCreativeTab(CreativeTabs.tabBlock);
    public static Block dragonEgg = Blocks.dragon_egg; //(new BlockDragonEgg(122)).setHardness(3.0F).setResistance(15.0F).setStepSound(soundStoneFootstep).setLightValue(0.125F).setUnlocalizedName("dragonEgg");
    public static Block redstoneLampIdle = Blocks.redstone_lamp; //(new BlockRedstoneLight(123, false)).setHardness(0.3F).setStepSound(soundGlassFootstep).setUnlocalizedName("redstoneLight").setCreativeTab(CreativeTabs.tabRedstone);
    public static Block redstoneLampActive = Blocks.lit_redstone_lamp; //(new BlockRedstoneLight(124, true)).setHardness(0.3F).setStepSound(soundGlassFootstep).setUnlocalizedName("redstoneLight");
    public static Block woodDoubleSlab = Blocks.double_wooden_slab; //(BlockHalfSlab)(new BlockWoodSlab(125, true)).setHardness(2.0F).setResistance(5.0F).setStepSound(soundWoodFootstep).setUnlocalizedName("woodSlab");
    public static Block woodSingleSlab = Blocks.wooden_slab; //(BlockHalfSlab)(new BlockWoodSlab(126, false)).setHardness(2.0F).setResistance(5.0F).setStepSound(soundWoodFootstep).setUnlocalizedName("woodSlab");
    public static Block cocoaPlant = Blocks.cocoa; //(new BlockCocoa(127)).setHardness(0.2F).setResistance(5.0F).setStepSound(soundWoodFootstep).setUnlocalizedName("cocoa");
    public static Block stairsSandStone = Blocks.sandstone_stairs; //(new BlockStairs(128, sandStone, 0)).setUnlocalizedName("stairsSandStone");
    public static Block oreEmerald = Blocks.emerald_ore; //(new BlockOre(129)).setHardness(3.0F).setResistance(5.0F).setStepSound(soundStoneFootstep).setUnlocalizedName("oreEmerald");
    public static Block enderChest = Blocks.ender_chest; //(new BlockEnderChest(130)).setHardness(22.5F).setResistance(1000.0F).setStepSound(soundStoneFootstep).setUnlocalizedName("enderChest").setLightValue(0.5F);
    public static Block tripWireSource = Blocks.tripwire_hook; //(BlockTripWireSource)(new BlockTripWireSource(131)).setUnlocalizedName("tripWireSource");
    public static Block tripWire = Blocks.tripwire; //(new BlockTripWire(132)).setUnlocalizedName("tripWire");
    public static Block blockEmerald = Blocks.emerald_block; //(new BlockOreStorage(133)).setHardness(5.0F).setResistance(10.0F).setStepSound(soundMetalFootstep).setUnlocalizedName("blockEmerald");
    public static Block stairsWoodSpruce = Blocks.spruce_stairs; //(new BlockStairs(134, planks, 1)).setUnlocalizedName("stairsWoodSpruce");
    public static Block stairsWoodBirch = Blocks.birch_stairs; //(new BlockStairs(135, planks, 2)).setUnlocalizedName("stairsWoodBirch");
    public static Block stairsWoodJungle = Blocks.jungle_stairs; //(new BlockStairs(136, planks, 3)).setUnlocalizedName("stairsWoodJungle");
    public static Block commandBlock = Blocks.command_block; //(new BlockCommandBlock(137)).setUnlocalizedName("commandBlock");
    public static Block beacon = Blocks.beacon; //(BlockBeacon)(new BlockBeacon(138)).setUnlocalizedName("beacon").setLightValue(1.0F);
    public static Block cobblestoneWall = Blocks.cobblestone_wall; //(new BlockWall(139, cobblestone)).setUnlocalizedName("cobbleWall");
    public static Block flowerPot = Blocks.flower_pot; //(new BlockFlowerPot(140)).setHardness(0.0F).setStepSound(soundPowderFootstep).setUnlocalizedName("flowerPot");
    public static Block carrot = Blocks.carrots; //(new BlockCarrot(141)).setUnlocalizedName("carrots");
    public static Block potato = Blocks.potatoes; //(new BlockPotato(142)).setUnlocalizedName("potatoes");
    public static Block woodenButton = Blocks.wooden_button; //(new BlockButtonWood(143)).setHardness(0.5F).setStepSound(soundWoodFootstep).setUnlocalizedName("button");
    public static Block skull = Blocks.skull; //(new BlockSkull(144)).setHardness(1.0F).setStepSound(soundStoneFootstep).setUnlocalizedName("skull");
    public static Block anvil = Blocks.anvil; //(new BlockAnvil(145)).setHardness(5.0F).setStepSound(soundAnvilFootstep).setResistance(2000.0F).setUnlocalizedName("anvil");
    public static Block chestTrapped = Blocks.trapped_chest; //(new BlockChest(146, 1)).setHardness(2.5F).setStepSound(soundWoodFootstep).setUnlocalizedName("chestTrap");
    public static Block pressurePlateGold = Blocks.light_weighted_pressure_plate; //(new BlockPressurePlateWeighted(147, "blockGold", Material.iron, 64)).setHardness(0.5F).setStepSound(soundWoodFootstep).setUnlocalizedName("weightedPlate_light");
    public static Block pressurePlateIron = Blocks.heavy_weighted_pressure_plate; //(new BlockPressurePlateWeighted(148, "blockIron", Material.iron, 640)).setHardness(0.5F).setStepSound(soundWoodFootstep).setUnlocalizedName("weightedPlate_heavy");
    public static Block redstoneComparatorIdle = Blocks.unpowered_comparator; //(BlockComparator)(new BlockComparator(149, false)).setHardness(0.0F).setStepSound(soundWoodFootstep).setUnlocalizedName("comparator").disableStats();
    public static Block redstoneComparatorActive = Blocks.powered_comparator; //(BlockComparator)(new BlockComparator(150, true)).setHardness(0.0F).setLightValue(0.625F).setStepSound(soundWoodFootstep).setUnlocalizedName("comparator").disableStats();
    public static Block daylightSensor = Blocks.daylight_detector; //(BlockDaylightDetector)(new BlockDaylightDetector(151)).setHardness(0.2F).setStepSound(soundWoodFootstep).setUnlocalizedName("daylightDetector");
    public static Block blockRedstone = Blocks.redstone_wire; //(new BlockPoweredOre(152)).setHardness(5.0F).setResistance(10.0F).setStepSound(soundMetalFootstep).setUnlocalizedName("blockRedstone");
    public static Block oreNetherQuartz = Blocks.quartz_ore; //(new BlockOre(153)).setHardness(3.0F).setResistance(5.0F).setStepSound(soundStoneFootstep).setUnlocalizedName("netherquartz");
    public static Block hopperBlock = Blocks.hopper; //(BlockHopper)(new BlockHopper(154)).setHardness(3.0F).setResistance(8.0F).setStepSound(soundWoodFootstep).setUnlocalizedName("hopper");
    public static Block blockNetherQuartz = Blocks.quartz_block; //(new BlockQuartz(155)).setStepSound(soundStoneFootstep).setHardness(0.8F).setUnlocalizedName("quartzBlock");
    public static Block stairsNetherQuartz = Blocks.nether_brick_stairs; //(new BlockStairs(156, blockNetherQuartz, 0)).setUnlocalizedName("stairsQuartz");
    public static Block railActivator = Blocks.activator_rail; //(new BlockRailPowered(157)).setHardness(0.7F).setStepSound(soundMetalFootstep).setUnlocalizedName("activatorRail");
    public static Block dropper = Blocks.dropper; //(new BlockDropper(158)).setHardness(3.5F).setStepSound(soundStoneFootstep).setUnlocalizedName("dropper");

    public static TileEntity getBlockTileEntity(IBlockAccess world, int x, int y, int z)
    {
        return world.getTileEntity(x, y, z);
    }

    public static void removeBlockTileEntity(World world, int x, int y, int z)
    {
        world.removeTileEntity(x, y, z);
    }

    public static Block getBlockFromItemBlock(ItemBlock item)
    {
        return item.field_150939_a;
    }

    public static Item getItemByIdEm(int id)
    {
        return Item.getItemById(id);
    }

    public static List<ItemStack> getDrops(World world, int x, int y, int z)
    {
        return world.getBlock(x, y, z).getDrops(world, x, y, z, world.getBlockMetadata(x, y, z), 0);
    }

    public static float getBlockHardness(Block block)
    {
        return block.getBlockHardness(null, 0, 0, 0);
    }

    public static Material getBlockMaterial(Block block)
    {
        return block.getMaterial();
    }

    public static void setBlockTileEntity(World world, int x, int y, int z, TileEntity tile)
    {
        world.setTileEntity(x, y, z, tile);
    }

    public static Block getBlockBId(int id)
    {
        return Block.getBlockById(id);
    }

    public static Block getBlockById(int id)
    {
        return Block.getBlockById(id);
    }

    public static boolean isBlockReplaceable(World world, int x, int y, int z, Block block)
    {
        return block.isReplaceable(world, x, y, z);
    }
}
