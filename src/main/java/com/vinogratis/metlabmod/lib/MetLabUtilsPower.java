package com.vinogratis.metlabmod.lib;

import com.vinogratis.metlabmod.common.energy.IPowerContainer;
import com.vinogratis.metlabmod.common.energy.IPowerGenerator;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import java.util.ArrayList;

public class MetLabUtilsPower
{
    public static int radius = 10;

    public static ArrayList<IPowerGenerator> generatorsList = new ArrayList<IPowerGenerator>();

    public static boolean tryToAddCharge(TileEntity tile, int power)
    {

        if (tile instanceof IPowerContainer)
        {
            if (((IPowerContainer) tile).getCharge() + power <= ((IPowerContainer) tile).getMaxCharge())
            {

                payPowerTo(tile, power);

                return true;

            }

        }

        return false;

    }

    public static boolean payPowerFromTo(TileEntity from, TileEntity to, int power)
    {

        IPowerContainer cFrom = (IPowerContainer)from;
        IPowerContainer cTo = (IPowerContainer)to;


        return payPowerFromTo(cFrom, cTo, power);
    }
    public static boolean payPowerFromTo(IPowerContainer cFrom, IPowerContainer cTo, int power)
    {


        if (cFrom.getCharge() >= power)
        {

            if (cTo.getCharge() + power <= cTo.getMaxCharge())
            {

                cFrom.setCharge(cFrom.getCharge() - power);
                cTo.setCharge(cTo.getCharge() + power);
                return true;

            }
            else
            {

                int payed = cTo.getCharge() + power - cTo.getMaxCharge();
                cFrom.setCharge(cFrom.getCharge() - payed);
                cTo.setCharge(cTo.getCharge() + payed);
                return true;

            }

        }

        return false;

    }

    public static boolean payPowerTo(TileEntity tile, int power)
    {
        //EldrichMagic.logger.line();
        //EldrichMagic.logger.ClientMessage("Try to pay "+mana+" mana to tile "+tile);
        World world = tile.getWorldObj();
        int x = tile.xCoord, y = tile.yCoord, z = tile.zCoord;
        int i1;
        ArrayList tiles = findAllPowerTileEntities(world, x, y, z, radius, true);
        IPowerContainer ent;
        IPowerContainer containerToPay;

        for (i1 = 0; tiles != null && i1<= tiles.size() - 1; i1++)
        {

            ent = (IPowerContainer)tiles.get(i1);
            containerToPay = (IPowerContainer)tile;
            //EldrichMagic.logger.ClientMessage("    to: "+containerToPay+", from "+ent);
            if (ent.getCharge() >= power && ((TileEntity)tiles.get(i1)) != tile && ent.canPayPower())
            {

                if (containerToPay.getCharge() + power <= containerToPay.getMaxCharge())
                {

                    ent.setCharge(ent.getCharge() - power);
                    containerToPay.setCharge(containerToPay.getCharge() + power);
                    //EldrichMagic.logger.ClientMessage("    sucsessfull");
                    return true;

                }
                else
                {

                    int payedCharge = containerToPay.getCharge() + power - containerToPay.getMaxCharge();
                    ent.setCharge(ent.getCharge() - payedCharge);
                    containerToPay.setCharge(containerToPay.getCharge() + payedCharge);
                    //EldrichMagic.logger.ClientMessage("    sucsessfull");
                    return true;

                }


            }

        }

        return false;

    }

    public static ArrayList findAllPowerTileEntities(World world, int x, int y, int z, int radius, boolean withPower)
    {

        ArrayList allTiles = MetLabUtils.findAllTileEntities(world, x, y, z, radius);
        ArrayList ret = new ArrayList();
        TileEntity tile;
        int i1;
        boolean debug = false;
        //EldrichMagic.logger.ClientMessage("TileEntityList = "+allTiles);
        for (i1 = 0; allTiles != null && i1<=allTiles.size() - 1 && !debug; i1++)
        {

            tile = (TileEntity)allTiles.get(i1);
            if (withPower && tile instanceof IPowerContainer)
            {

                IPowerContainer powerTile = (IPowerContainer)tile;
                if (powerTile.getCharge() > 0) ret.add(tile);


            }
            else if (tile instanceof IPowerContainer) ret.add(tile);

        }

        //EldrichMagic.logger.ClientMessage("Mana TileEntityList = "+ret);
        if (ret.size() > 0) return ret;
        return null;


    }
}
