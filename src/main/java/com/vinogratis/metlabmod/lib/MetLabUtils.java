package com.vinogratis.metlabmod.lib;

import com.vinogratis.metlabmod.client.helper.UtilsClient;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

import java.util.ArrayList;

public class MetLabUtils
{
    public static MetLabUtilsPower powerUtils;
    public static UtilsClient client;
    public static MetLabUtilsInventory inventory;
    public static MetLabUtilsNBT nbt = new MetLabUtilsNBT();


    public static ArrayList findAllTileEntities(World world, int x, int y, int z, int radius)
    {

        TileEntity tile;
        ArrayList ret = new ArrayList();
        int sx, sy, sz;
        int minX = x - radius, minY = y - radius, minZ = z - radius;
        int maxX = x + radius, maxY = y + radius, maxZ = z + radius;

        for (sx = minX; sx <= maxX; sx++)
        {

            for (sy = minY; sy <= maxY; sy++)
            {

                for (sz = minZ; sz <= maxZ; sz++)
                {

                    tile = MetLabUtilsBlocks.getBlockTileEntity(world, sx, sy, sz);
                    //EldrichMagic.logger.ClientMessage("TileEntityAt"+sx+""+sy+""+sz+" = "+tile+". Allowed: "+(tile != null));
                    if (tile != null)
                        ret.add(tile);

                }

            }

        }

        if (ret.size() > 0)
            return ret;
        return null;

    }
    public static int transformateToPixels(int maximum, int num, int textureSize)
    {

        if (num == 0) return 0;
        return MathHelper.floor_double((double)((float)num / (float)maximum) * textureSize);

    }
}
