package com.vinogratis.metlabmod.lib;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

import java.util.Random;

public class MetLabUtilsInventory
{
    public static ItemStack getStackInSlot(int par1, ItemStack[] inv)
    {
        return inv[par1];
    }

    public static ItemStack decrStackSizeCResult(int fromSlot, int toSlot, ItemStack[] inv)
    {
        if (inv[fromSlot] != null)
        {
            ItemStack itemstack = inv[fromSlot];
            inv[fromSlot] = null;
            return itemstack;
        }
        else
        {
            return null;
        }
    }

    /**
     * Removes from an inventory slot (first arg) up to a specified number (second arg) of items and returns them in a
     * new stack.
     */
    public static ItemStack decrStackSize(int par1, int par2, ItemStack[] inv)
    {
        if (inv[par1] != null)
        {
            ItemStack itemstack;

            if (inv[par1].stackSize <= par2)
            {
                itemstack = inv[par1];
                inv[par1] = null;
                return itemstack;
            }
            else
            {
                itemstack = inv[par1].splitStack(par2);

                if (inv[par1].stackSize == 0)
                {
                    inv[par1] = null;
                }

                return itemstack;
            }
        }
        else
        {
            return null;
        }
    }

    /**
     * When some containers are closed they call this on each slot, then drop whatever it returns as an EntityItem -
     * like when you close a workbench GUI.
     */
    public ItemStack getStackInSlotOnClosing(int par1, ItemStack[] inv)
    {
        if (inv[par1] != null)
        {
            ItemStack itemstack = inv[par1];
            inv[par1] = null;
            return itemstack;
        }
        else
        {
            return null;
        }
    }

    /**
     * Sets the given item stack to the specified slot in the inventory (can be crafting or armor sections).
     */
    public void setInventorySlotContents(int par1, ItemStack par2ItemStack, ItemStack[] inv, IInventory iinv)
    {
        inv[par1] = par2ItemStack;

        if (par2ItemStack != null && par2ItemStack.stackSize > iinv.getInventoryStackLimit())
        {
            par2ItemStack.stackSize = iinv.getInventoryStackLimit();
        }
    }

    public static ItemStack insertStack(IInventory par1IInventory, ItemStack par2ItemStack, int par3)
    {
        if (par1IInventory instanceof ISidedInventory && par3 > -1)
        {
            ISidedInventory isidedinventory = (ISidedInventory)par1IInventory;
            int[] aint = isidedinventory.getAccessibleSlotsFromSide(par3);

            for (int j = 0; j < aint.length && par2ItemStack != null && par2ItemStack.stackSize > 0; ++j)
            {
                par2ItemStack = func_102014_c(par1IInventory, par2ItemStack, aint[j], par3);
            }
        }
        else
        {
            int k = par1IInventory.getSizeInventory();

            for (int l = 0; l < k && par2ItemStack != null && par2ItemStack.stackSize > 0; ++l)
            {
                par2ItemStack = func_102014_c(par1IInventory, par2ItemStack, l, par3);
            }
        }

        if (par2ItemStack != null && par2ItemStack.stackSize == 0)
        {
            par2ItemStack = null;
        }

        return par2ItemStack;
    }

    private static ItemStack func_102014_c(IInventory par0IInventory, ItemStack par1ItemStack, int par2, int par3)
    {
        ItemStack itemstack1 = par0IInventory.getStackInSlot(par2);

        if (func_102015_a(par0IInventory, par1ItemStack, par2, par3))
        {
            boolean flag = false;

            if (itemstack1 == null)
            {
                par0IInventory.setInventorySlotContents(par2, par1ItemStack);
                par1ItemStack = null;
                flag = true;
            }
            else if (areItemStacksEqualItem(itemstack1, par1ItemStack))
            {
                int k = par1ItemStack.getMaxStackSize() - itemstack1.stackSize;
                int l = Math.min(par1ItemStack.stackSize, k);
                par1ItemStack.stackSize -= l;
                itemstack1.stackSize += l;
                flag = l > 0;
            }

            if (flag)
            {
                par0IInventory.markDirty();
            }
        }

        return par1ItemStack;
    }

    private static boolean func_102015_a(IInventory par0IInventory, ItemStack par1ItemStack, int par2, int par3)
    {
        return !par0IInventory.isItemValidForSlot(par2, par1ItemStack) ? false : !(par0IInventory instanceof ISidedInventory) || ((ISidedInventory)par0IInventory).canInsertItem(par2, par1ItemStack, par3);
    }

    private static boolean areItemStacksEqualItem(ItemStack par1ItemStack, ItemStack par2ItemStack)
    {
        return par1ItemStack.getItem() != par2ItemStack.getItem() ? false : (par1ItemStack.getItemDamage() != par2ItemStack.getItemDamage() ? false : (par1ItemStack.stackSize > par1ItemStack.getMaxStackSize() ? false : ItemStack.areItemStackTagsEqual(par1ItemStack, par2ItemStack)));
    }

    /**
     *  I keep this from Thaumcraft 3.0.5a. Thanks to Authors.
     */
    public static void dropItems(World world, int x, int y, int z)
    {
        Random rand = new Random();
        int md = world.getBlockMetadata(x, y, z);
        TileEntity tileEntity = MetLabUtilsBlocks.getBlockTileEntity(world, x, y, z);
        if(tileEntity instanceof IInventory) {
            IInventory inventory = (IInventory)tileEntity;

            float rz;
            float factor;
            for(int rx = 0; rx < inventory.getSizeInventory(); ++rx) {
                ItemStack ry = inventory.getStackInSlot(rx);
                if(ry != null && ry.stackSize > 0) {
                    rz = rand.nextFloat() * 0.8F + 0.1F;
                    float entityItem = rand.nextFloat() * 0.8F + 0.1F;
                    factor = rand.nextFloat() * 0.8F + 0.1F;
                    EntityItem entityItem1 = new EntityItem(world, (double)((float)x + rz), (double)((float)y + entityItem), (double)((float)z + factor), new ItemStack(ry.getItem(), ry.stackSize, ry.getItemDamage()));
                    if(ry.hasTagCompound()) {
                        entityItem1.getEntityItem().setTagCompound((NBTTagCompound)ry.getTagCompound().copy());
                    }

                    float factor1 = 0.05F;
                    entityItem1.motionX = rand.nextGaussian() * (double)factor1;
                    entityItem1.motionY = rand.nextGaussian() * (double)factor1 + 0.20000000298023224D;
                    entityItem1.motionZ = rand.nextGaussian() * (double)factor1;
                    world.spawnEntityInWorld(entityItem1);
                    ry.stackSize = 0;
                }
            }


        }
    }



    /**
     *  I keep this from Thaumcraft 3.0.5a. Thanks to Authors.
     */
    public static MovingObjectPosition getTargetBlock(World world, EntityPlayer player, boolean par3)
    {
        float var4 = 1.0F;
        float var5 = player.prevRotationPitch + (player.rotationPitch - player.prevRotationPitch) * var4;
        float var6 = player.prevRotationYaw + (player.rotationYaw - player.prevRotationYaw) * var4;
        double var7 = player.prevPosX + (player.posX - player.prevPosX) * (double)var4;
        double var9 = player.prevPosY + (player.posY - player.prevPosY) * (double)var4 + 1.62D - (double)player.yOffset;
        double var11 = player.prevPosZ + (player.posZ - player.prevPosZ) * (double)var4;
        Vec3 var13 = /*world.getWorldVec3Pool().getVecFromPool(var7, var9, var11);*/ null;
        float var14 = MathHelper.cos(-var6 * 0.017453292F - 3.1415927F);
        float var15 = MathHelper.sin(-var6 * 0.017453292F - 3.1415927F);
        float var16 = -MathHelper.cos(-var5 * 0.017453292F);
        float var17 = MathHelper.sin(-var5 * 0.017453292F);
        float var18 = var15 * var16;
        float var20 = var14 * var16;
        double var21 = 10.0D;
        Vec3 var23 = var13.addVector((double)var18 * var21, (double)var17 * var21, (double)var20 * var21);
        return world.rayTraceBlocks(var13, var23, par3);
    }

    public static void dropItems(World world, int x, int y, int z, ItemStack[] toDrop)
    {
        Random rand = new Random();
        int md = world.getBlockMetadata(x, y, z);
        TileEntity tileEntity = MetLabUtilsBlocks.getBlockTileEntity(world, x, y, z);
        float rz;
        float factor;
        for(int rx = 0; rx < toDrop.length; ++rx) {
            ItemStack ry = toDrop[rx];
            if(ry != null && ry.stackSize > 0) {
                rz = rand.nextFloat() * 0.8F + 0.1F;
                float entityItem = rand.nextFloat() * 0.8F + 0.1F;
                factor = rand.nextFloat() * 0.8F + 0.1F;
                EntityItem entityItem1 = new EntityItem(world, (double)((float)x + rz), (double)((float)y + entityItem), (double)((float)z + factor), new ItemStack(ry.getItem(), ry.stackSize, ry.getItemDamage()));
                if(ry.hasTagCompound()) {
                    entityItem1.getEntityItem().setTagCompound((NBTTagCompound)ry.getTagCompound().copy());
                }

                float factor1 = 0.05F;
                entityItem1.motionX = rand.nextGaussian() * (double)factor1;
                entityItem1.motionY = rand.nextGaussian() * (double)factor1 + 0.20000000298023224D;
                entityItem1.motionZ = rand.nextGaussian() * (double)factor1;
                world.spawnEntityInWorld(entityItem1);
                ry.stackSize = 0;
            }
        }
    }
}
