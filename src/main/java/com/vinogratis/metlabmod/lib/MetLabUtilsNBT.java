package com.vinogratis.metlabmod.lib;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

public class MetLabUtilsNBT
{
    public static void writeInventoryToNBT(NBTTagCompound tag, ItemStack[] inventory, String name)
    {
        NBTTagList list = new NBTTagList();

        for (int i1 = 0; i1 <= inventory.length - 1 && inventory.length != 0; i1++)
        {

            if (inventory[i1] != null)
            {

                NBTTagCompound item = new NBTTagCompound();
                item.setByte("Slot", (byte) i1);
                inventory[i1].writeToNBT(item);
                list.appendTag(item);

            }

        }

        tag.setTag(name, list);
    }

    public ItemStack[] readInventoryFromNBT(NBTTagCompound tag, String name, int invLenth)
    {
        ItemStack[] inventory = null;

        if (tag.hasKey(name))
        {

            NBTTagList list = tag.getTagList(name, 10);

            inventory = new ItemStack[invLenth];

            for (int i1 = 0; i1 <= list.tagCount() - 1 && list.tagCount() != 0; i1++)
            {

                NBTTagCompound item = (NBTTagCompound) list.getCompoundTagAt(i1);
                int slot = item.getByte("Slot");

                inventory[slot] = ItemStack.loadItemStackFromNBT(item);

            }

        }

        return inventory;
    }

}
