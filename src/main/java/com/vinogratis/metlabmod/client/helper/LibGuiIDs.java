package com.vinogratis.metlabmod.client.helper;

public class LibGuiIDs
{
    public static int next = 0;

    public static final int chestGuiId = 0;
    public static final int repairerGuiId = 1;
    public static final int generatorGuiId = 2;
    public static final int normalGeneratorGuiId = 3;
    public static final int oven = 4;
}
