package com.vinogratis.metlabmod.client.helper;

import com.vinogratis.metlabmod.client.gui.GuiMetLabFunace;
import com.vinogratis.metlabmod.client.gui.GuiNormalGenerator;
import com.vinogratis.metlabmod.client.gui.GuiOven;
import com.vinogratis.metlabmod.client.gui.GuiRepairer;
import com.vinogratis.metlabmod.common.inventory.container.ContainerGenerator;
import com.vinogratis.metlabmod.common.inventory.container.ContainerMetLabFurnace;
import com.vinogratis.metlabmod.common.inventory.container.ContainerNormalGenerator;
import com.vinogratis.metlabmod.common.inventory.container.ContainerOven;
import com.vinogratis.metlabmod.common.inventory.container.ContainerRepairer;
import com.vinogratis.metlabmod.common.inventory.container.ChestContainer;
import com.vinogratis.metlabmod.client.gui.GuiChest;
import com.vinogratis.metlabmod.common.tileentities.TileGenerator;
import com.vinogratis.metlabmod.common.tileentities.TileMetLabFurnace;
import com.vinogratis.metlabmod.common.tileentities.TileNormalGenerator;
import com.vinogratis.metlabmod.common.tileentities.TileOven;
import com.vinogratis.metlabmod.common.tileentities.TileRepairer;
import cpw.mods.fml.common.network.IGuiHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class GuiHandler implements IGuiHandler
{
    @Override
    public Object getServerGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
    {
        TileEntity tile = world.getTileEntity(x, y, z);

        switch (id)
        {
        case LibGuiIDs.oven:
            return new ContainerOven(player, (TileOven) tile);

        case LibGuiIDs.normalGeneratorGuiId:
            return new ContainerNormalGenerator(player, (TileNormalGenerator) tile);

        case LibGuiIDs.chestGuiId:
            return new ChestContainer(player);

        case LibGuiIDs.repairerGuiId:
            return new ContainerRepairer(player, (TileRepairer) tile);
        }

        return null;
    }

    @Override
    public Object getClientGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
    {
        TileEntity tile = world.getTileEntity(x, y, z);

        switch (id)
        {
        case LibGuiIDs.oven:
            return new GuiOven(player, (TileOven) tile);
        case LibGuiIDs.normalGeneratorGuiId:
            return new GuiNormalGenerator(player, (TileNormalGenerator) tile);
        case LibGuiIDs.generatorGuiId:
            return new ContainerGenerator(player, (TileGenerator) tile);
        case LibGuiIDs.chestGuiId:
            return new GuiChest(player);
        case LibGuiIDs.repairerGuiId:
            return new GuiRepairer(player, (TileRepairer) tile);
        }

        return null;
    }
}
