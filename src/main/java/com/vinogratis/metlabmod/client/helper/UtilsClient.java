package com.vinogratis.metlabmod.client.helper;

import lombok.var;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.oredict.OreDictionary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class UtilsClient
{
    protected static RenderItem itemRenderer = new RenderItem();
    public static HashMap<String, ResourceLocation> bindedTextures = new HashMap();

    public static void drawTexturedModalRectScaled(int x, int y, int u, int v, int tx, int ty, float scalef)
    {

        float f = 0.00390625F;
        float f1 = 0.00390625F;
        Tessellator tessellator = Tessellator.instance;

        int scale = MathHelper.floor_float(scalef * 10 / 3 * 5);
        //GL11.glTranslatef(-x, -y, 0);
        //GL11.glScalef(1.0F + scale, 1.0F + scale, 1.0F + scale);
        tessellator.startDrawingQuads();
        tessellator.addVertexWithUV((double)(x + 0 - scale), (double)(y + ty + scale), 0, (double)((float)(u + 0) * f), (double)((float)(v + ty) * f1));
        tessellator.addVertexWithUV((double)(x + tx + scale), (double)(y + ty + scale),0, (double)((float)(u + tx) * f), (double)((float)(v + ty) * f1));
        tessellator.addVertexWithUV((double)(x + tx + scale), (double)(y + 0 - scale), 0, (double)((float)(u + tx) * f), (double)((float)(v + 0) * f1));
        tessellator.addVertexWithUV((double)(x + 0 - scale), (double)(y + 0 - scale), 0, (double)((float)(u + 0) * f), (double)((float)(v + 0) * f1));
        tessellator.draw();

    }
    public static int transformateToPixels(int maximum, int num, int textureSize)
    {

        if (num == 0) return 0;
        return MathHelper.floor_double((double)((float)num / (float)maximum) * textureSize);

    }
    public static void bindTexture(String texturename)
    {

        if (bindedTextures.get(texturename) != null)
        {
            Minecraft.getMinecraft().renderEngine.bindTexture(bindedTextures.get(texturename));
        }
        else
        {
            String domainPrefix = "metlabmod";

            if (texturename.contains(":"))
            {
                var indexOfSplitter = texturename.indexOf(':');
                domainPrefix = texturename.substring(0, indexOfSplitter);
                texturename = texturename.substring(indexOfSplitter + 1);
            }
            if (!(texturename.startsWith("textures") || texturename.startsWith("/textures")))
            {
                texturename = "textures"+(texturename.startsWith("/") ? "" : "/")+texturename;
            }

            ResourceLocation rl = new ResourceLocation(domainPrefix, texturename);

            bindedTextures.put(texturename, rl);

            Minecraft.getMinecraft().renderEngine.bindTexture(rl);
        }
    }


    public static ItemStack getRandomStack(Object obj)
    {
        ItemStack ret = null;
        Random rand = new Random();

        if (obj != null)
        {

            if (obj instanceof ItemStack)
            {
                ItemStack stack = (ItemStack)obj;

                if (stack.getItemDamage() == 32767)
                {

                    if (stack.getItem() instanceof ItemBlock)
                    {

                        ItemBlock iBlock = (ItemBlock)stack.getItem();
                        List items = new ArrayList();

                        iBlock.getSubItems(iBlock, iBlock.getCreativeTab(), items);

                        int id = (int)(System.currentTimeMillis() / 1000L % (long)items.size());



                        ret = (ItemStack)items.get(id);




                    }

                    else
                    {

                        List items = new ArrayList();

                        stack.getItem().getSubItems(stack.getItem(), stack.getItem().getCreativeTab(), items);

                        int id = (int)(System.currentTimeMillis() / 1000L % (long)items.size());

                        ret = (ItemStack)items.get(id);

                    }

                }
                else
                    ret = (ItemStack)obj;

            }
            else if (obj instanceof ArrayList)
            {

                ArrayList<ItemStack> list = (ArrayList<ItemStack>)obj;

                int ind = rand.nextInt(list.size());

                ret = list.get(ind);


            }
            else if (obj instanceof String)
            {

                String name = (String)obj;
                ArrayList<ItemStack> list = new ArrayList<ItemStack>();

                list = OreDictionary.getOres(name);

                int id = (int)(System.currentTimeMillis() / 1000L % (long)list.size());

                ret = (ItemStack)list.get(id);

            }

        }



        return ret;
    }
}
