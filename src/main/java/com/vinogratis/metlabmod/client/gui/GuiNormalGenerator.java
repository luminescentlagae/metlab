package com.vinogratis.metlabmod.client.gui;

import com.vinogratis.metlabmod.client.helper.UtilsClient;
import com.vinogratis.metlabmod.common.inventory.container.ContainerNormalGenerator;
import com.vinogratis.metlabmod.common.tileentities.TileNormalGenerator;
import com.vinogratis.metlabmod.lib.MetLabUtils;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;

public class GuiNormalGenerator extends GuiContainer
{

    public TileNormalGenerator tile;
    public static int centerX, centerY;

    public GuiNormalGenerator(EntityPlayer player, TileNormalGenerator tile)
    {
        super(new ContainerNormalGenerator(player,tile));

        this.tile = tile;

        centerX = this.width / 2;
        centerY = this.height / 2;

    }

    @Override
    public void drawGuiContainerBackgroundLayer(float f, int i, int j)
    {
        centerX = this.width / 2 - 40;
        centerY = this.height / 2 - 55;

        drawPlayerInventory();
        drawGenerator();
    }

    public void drawGenerator()
    {
        int energy = MetLabUtils.transformateToPixels(tile.getMaxCharge(), tile.getCharge(), 59);
        // Серая полоска
        UtilsClient.drawTexturedModalRectScaled(centerX - 46 - 22, centerY + 25, 136, 22, 59, 6, 0.0F);

        // Зеленая полоска
        UtilsClient.drawTexturedModalRectScaled(centerX - 46 - 22, centerY + 25, 136, 15, energy, 6, 0.0F);

        // Надпись "Energy"
        UtilsClient.drawTexturedModalRectScaled(centerX - 46 - 22, centerY, 136, 30, 58, 13, 0.0F);
    }



    public void drawPlayerInventory()
    {
        int dx = this.width / 2 - 176 + 89 - 1;
        int dy = this.height / 2 - 85 + 103 - 40 + 1;

        this.drawTexturedModalRect(dx, dy + 20, 0, 81, 176, 85);
    }

}