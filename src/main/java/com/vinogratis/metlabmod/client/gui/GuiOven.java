package com.vinogratis.metlabmod.client.gui;

import com.vinogratis.metlabmod.client.helper.UtilsClient;
import com.vinogratis.metlabmod.common.inventory.container.ContainerOven;
import com.vinogratis.metlabmod.common.tileentities.TileOven;
import com.vinogratis.metlabmod.lib.MetLabUtils;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;

@SideOnly(Side.CLIENT)
public class GuiOven extends GuiContainer
{
    public TileOven tile;
    public static int centerX, centerY;
    private static final ResourceLocation furnaceGuiTextures = new ResourceLocation("textures/gui/container/furnace.png");

    public GuiOven(EntityPlayer player, TileOven tile)
    {
        super(new ContainerOven(player, tile));

        this.tile = tile;

        centerX = this.width / 2;
        centerY = this.height / 2;

    }

    @Override
    public void drawGuiContainerBackgroundLayer(float f, int i, int j)
    {
        centerX = this.width / 2 - 40;
        centerY = this.height / 2 - 55;

        drawPlayerInventory();
        drawFurnace();
    }

    public void drawFurnace()
    {
        UtilsClient.bindTexture("textures/gui/magicFurnace.png");

        int progress = MetLabUtils.transformateToPixels(tile.maxBurnTime, tile.burnTime, 26);
        int energy = MetLabUtils.transformateToPixels(tile.getMaxCharge(), tile.getCharge(), 59);

        // Слот инпута
        UtilsClient.drawTexturedModalRectScaled(centerX, centerY, 11, 11, 20, 20, 0.1F);

        // Слот инпута
        UtilsClient.drawTexturedModalRectScaled(centerX + 60, centerY, 11, 11, 20, 20, 0.1F);

        // Серая полоска
        UtilsClient.drawTexturedModalRectScaled(centerX - 46 - 22, centerY + 25, 136, 22, 59, 6, 0.0F);

        // Зеленая полоска
        UtilsClient.drawTexturedModalRectScaled(centerX - 46 - 22, centerY + 25, 136, 15, energy, 6, 0.0F);

        // Надпись "Energy"
        UtilsClient.drawTexturedModalRectScaled(centerX - 46 - 22, centerY, 136, 30, 58, 13, 0.0F);

        // Стрелка (Пустая)
        UtilsClient.drawTexturedModalRectScaled(centerX + 27, centerY + 6, 39, 17, 26, 8, 0.0F);

        // Стрелка (Заполнена)
        UtilsClient.drawTexturedModalRectScaled(centerX + 27, centerY + 6, 68, 17, 26 - progress, 8, 0.0F);

    }

    public void drawPlayerInventory()
    {
        //        this.mc.getTextureManager().bindTexture(furnaceGuiTextures);
        UtilsClient.bindTexture("minecraft:gui/container/inventory.png");

        int dx = this.width / 2 - 176 + 89 - 1;
        int dy = this.height / 2 - 85 + 103 - 40 + 1;

        this.drawTexturedModalRect(dx, dy + 20, 0, 81, 176, 85);
    }

}
