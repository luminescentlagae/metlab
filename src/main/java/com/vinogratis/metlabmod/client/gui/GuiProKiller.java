package com.vinogratis.metlabmod.client.gui;

import com.MetLabMod;
import com.vinogratis.metlabmod.common.inventory.container.ContainerProKiller;
import com.vinogratis.metlabmod.common.tileentities.TileProKiller;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;

public class GuiProKiller extends GuiContainer
{
    public static final ResourceLocation bgTextureLoc = new ResourceLocation(MetLabMod.MODID, "textures/gui/repairer.png");

    public GuiProKiller(EntityPlayer player, TileProKiller tileProKiller)
    {
        super(new ContainerProKiller(player, tileProKiller));
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
    {
        int textureW = 0;
        int textureH = 0;

        int bgStartX = (width - textureW) / 2;
        int bgStartY = (height - textureH) / 2;

        Minecraft.getMinecraft().getTextureManager().bindTexture(bgTextureLoc);
        drawTexturedModalRect(bgStartX, bgStartY, 0, 0, 256, 256);
    }
}
