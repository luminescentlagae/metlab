package com.vinogratis.metlabmod.client.gui;

import com.vinogratis.metlabmod.common.inventory.container.ContainerMetLabFurnace;
import com.vinogratis.metlabmod.common.tileentities.TileMetLabFurnace;
import com.vinogratis.metlabmod.lib.MetLabUtils;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class GuiMetLabFunace extends GuiContainer
{
    private static final ResourceLocation furnaceGuiTextures = new ResourceLocation("textures/gui/container/furnace.png");

    public TileMetLabFurnace tile;
    public static int centerX, centerY;

    public GuiMetLabFunace(EntityPlayer player, TileMetLabFurnace tile)
    {
        super(new ContainerMetLabFurnace(player.inventory, tile));

        this.tile = tile;

        this.centerX = this.width / 2;
        this.centerY = this.height / 2;

    }
    protected void drawGuiContainerForegroundLayer(int par1, int par2)
    {
        String string = this.tile.hasCustomInventoryName() ? this.tile.getInventoryName() : I18n.format(this.tile.getInventoryName(), new Object[0]);
        this.fontRendererObj.drawString(string, this.xSize / 2 - this.fontRendererObj.getStringWidth(string), 6, 4210752);
        this.fontRendererObj.drawString(I18n.format("container.inventory", new Object[0]), 8, this.ySize - 94, 4210752);
    }


    public void drawFurnace()
    {
        int progress = MetLabUtils.transformateToPixels(tile.maxBurnTime, tile.burnTime, 26);
        int energy = MetLabUtils.transformateToPixels(tile.getMaxCharge(), tile.getCharge(), 59);

        // Слот инпута
        MetLabUtils.client.drawTexturedModalRectScaled(centerX, centerY, 11, 11, 20, 20, 0.1F);

        // Слот инпута
        MetLabUtils.client.drawTexturedModalRectScaled(centerX + 60, centerY, 11, 11, 20, 20, 0.1F);

        // Серая полоска
        MetLabUtils.client.drawTexturedModalRectScaled(centerX - 46 - 22, centerY + 25, 136, 22, 59, 6, 0.0F);

        // Зеленая полоска
        MetLabUtils.client.drawTexturedModalRectScaled(centerX - 46 - 22, centerY + 25, 136, 15, energy, 6, 0.0F);

        // Надпись "Energy"
        MetLabUtils.client.drawTexturedModalRectScaled(centerX - 46 - 22, centerY, 136, 30, 58, 13, 0.0F);

        // Стрелка (Пустая)
        MetLabUtils.client.drawTexturedModalRectScaled(centerX + 27, centerY + 6, 39, 17, 26, 8, 0.0F);

        // Стрелка (Заполнена)
        MetLabUtils.client.drawTexturedModalRectScaled(centerX + 27, centerY + 6, 68, 17, 26 - progress, 8, 0.0F);

    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3)
    {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(furnaceGuiTextures);
        int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
        int i1;

        if (this.tile.isBurning())
        {
            i1 = this.tile.getBurnTimeRemainingScaled(12);
            this.drawTexturedModalRect(k + 56, l + 36 + 12 - i1, 176, 12 - i1, 14, i1 + 2);
        }

        i1 = this.tile.getCookProgressScaled(24);
        this.drawTexturedModalRect(k + 79, l + 34, 176, 14, i1 + 1, 16);
    }

    public void drawPlayerInventory()
    {
        MetLabUtils.client.bindTexture("/gui/inventory.png");

        int dx = this.width / 2 - 176 + 89 - 1;
        int dy = this.height / 2 - 85 + 103 - 40 + 1;

        this.drawTexturedModalRect(dx, dy + 20, 0, 81, 176, 85);
    }

}


