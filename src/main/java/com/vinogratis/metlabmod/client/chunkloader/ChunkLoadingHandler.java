package com.vinogratis.metlabmod.client.chunkloader;

import com.vinogratis.metlabmod.config.MetLabBlocks;
import net.minecraft.block.Block;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.common.ForgeChunkManager.OrderedLoadingCallback;
import net.minecraftforge.common.ForgeChunkManager.Ticket;

import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;

public class ChunkLoadingHandler implements OrderedLoadingCallback
{

    @Override
    public List<ForgeChunkManager.Ticket> ticketsLoaded(List<Ticket> tickets, World world, int maxTicketCount) {
        List<Ticket> validTickets = new ArrayList<>();
        for (Ticket ticket : tickets) {
            int coreX = ticket.getModData().getInteger("coreX");
            int coreY = ticket.getModData().getInteger("coreY");
            int coreZ = ticket.getModData().getInteger("coreZ");
            Block blockChunkLoader = world.getBlock(coreX, coreY, coreZ);
            if (blockChunkLoader == MetLabBlocks.chunkLoader)
                validTickets.add(ticket);
        }
        return validTickets;
    }

    @Override
    public void ticketsLoaded(List<ForgeChunkManager.Ticket> tickets, World world) {
        for (Ticket ticket : tickets) {
            int coreX = ticket.getModData().getInteger("coreX");
            int coreY = ticket.getModData().getInteger("coreY");
            int coreZ = ticket.getModData().getInteger("coreZ");
            TileChunkLoader te = (TileChunkLoader) world.getTileEntity(coreX, coreY, coreZ);
            te.forceChunkLoadingCallback(ticket);
        }
    }
}