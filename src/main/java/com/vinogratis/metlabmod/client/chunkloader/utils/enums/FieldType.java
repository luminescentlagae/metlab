package com.vinogratis.metlabmod.client.chunkloader.utils.enums;
public enum FieldType {
    CHUNKLOADINGTIME,
    ISPAUSED,
    MODE;

    public static FieldType fromInteger(int x) {
        switch(x) {
        case 1:
            return CHUNKLOADINGTIME;
        case 2:
            return ISPAUSED;
        case 3:
            return MODE;
        }
        return null;
    }
}