package com.vinogratis.metlabmod.client.chunkloader.utils;

import com.vinogratis.metlabmod.config.MetLabBlocks;
import cpw.mods.fml.common.IWorldGenerator;
import net.minecraft.block.Block;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;

import java.util.Random;

public class BlockMyaGenerator implements IWorldGenerator
{
    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider)
    {
        switch (world.provider.dimensionId)
        {
        case -1:
            generateNether(world, random, chunkX * 16, chunkZ * 16);
        case 0:
            generateSurface(world, random, chunkX * 16, chunkZ * 16);
        case 1:
            generateEnd(world, random, chunkX * 16, chunkZ * 16);
        }
    }

    private void generateNether(World world, Random random, int x, int y)
    {

    }

    private void generateSurface(World world, Random random, int x, int y)
    {
        this.addOreSpawn(MetLabBlocks.repairer, world, random, x, y, 16, 16, 3 + random.nextInt(2), 7, 0, 30);

    }

    private void generateEnd(World world, Random random, int x, int y)
    {

    }

    public void addOreSpawn(Block block, World world, Random random, int blockXPos, int blockZPos, int maxX, int maxZ, int maxVeinSize, int chancesToSpawn, int minY, int maxY)
    {
        for (int i = 0; i < chancesToSpawn; i++)
        {
            int posX = blockXPos + random.nextInt(maxX);
            int posY = minY + random.nextInt(maxY - minY);
            int posZ = blockZPos + random.nextInt(maxZ);

            System.out.println("World gen mineable applied to " + blockXPos + " " + blockZPos + " in world " + world.provider.dimensionId);
            (new WorldGenMineable2(block, maxVeinSize)).generate(world, random, posX, posY, posZ);
        }
    }
}