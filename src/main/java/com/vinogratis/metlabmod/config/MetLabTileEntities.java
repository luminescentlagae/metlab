package com.vinogratis.metlabmod.config;

import com.MetLabMod;
import com.vinogratis.metlabmod.client.chunkloader.TileChunkLoader;
import com.vinogratis.metlabmod.common.tileentities.TileDegenerator;
import com.vinogratis.metlabmod.common.tileentities.TileGenerator;
import com.vinogratis.metlabmod.common.tileentities.TileKiller;
import com.vinogratis.metlabmod.common.tileentities.TileLevitator;
import com.vinogratis.metlabmod.common.tileentities.TileMagnet;
import com.vinogratis.metlabmod.common.tileentities.TileMetLabFurnace;
import com.vinogratis.metlabmod.common.tileentities.TileMobBarrier;
import com.vinogratis.metlabmod.common.tileentities.TileNormalGenerator;
import com.vinogratis.metlabmod.common.tileentities.TileOven;
import com.vinogratis.metlabmod.common.tileentities.TileProKiller;
import com.vinogratis.metlabmod.common.tileentities.TileQuarry;
import com.vinogratis.metlabmod.common.tileentities.TileRepairer;
import com.vinogratis.metlabmod.common.tileentities.TileStorage;
import cpw.mods.fml.common.registry.GameRegistry;

public class MetLabTileEntities
{
    public static void preInit()
    {
        registerEntities();
    }
    public static void registerEntities()
    {
        GameRegistry.registerTileEntity(TileChunkLoader.class, MetLabMod.MODID + "TileChunkLoader");
        GameRegistry.registerTileEntity(TileProKiller.class, MetLabMod.MODID + "TileProKiller");
        GameRegistry.registerTileEntity(TileOven.class, MetLabMod.MODID + "TileOven");
        GameRegistry.registerTileEntity(TileMetLabFurnace.class, MetLabMod.MODID + "TileMetLAbFurnace");
        GameRegistry.registerTileEntity(TileMobBarrier.class, MetLabMod.MODID + "TileMobBarrier");
        GameRegistry.registerTileEntity(TileLevitator.class, MetLabMod.MODID + "TileLevitator");
        GameRegistry.registerTileEntity(TileKiller.class, MetLabMod.MODID + "TileKiller");
        GameRegistry.registerTileEntity(TileRepairer.class, MetLabMod.MODID + "TileRepairer");
        GameRegistry.registerTileEntity(TileQuarry.class, MetLabMod.MODID + "TileQuarry");
        GameRegistry.registerTileEntity(TileMagnet.class, MetLabMod.MODID + "TileMagnet");
        GameRegistry.registerTileEntity(TileGenerator.class, MetLabMod.MODID + "TileGenerator");
        GameRegistry.registerTileEntity(TileDegenerator.class, MetLabMod.MODID + "TileDegenerator");
        GameRegistry.registerTileEntity(TileStorage.class, MetLabMod.MODID + "TileStorage");
        GameRegistry.registerTileEntity(TileNormalGenerator.class, MetLabMod.MODID + "TileNormalGenerator");
    }
}
