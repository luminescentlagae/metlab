package com.vinogratis.metlabmod.config;

import com.vinogratis.metlabmod.client.chunkloader.BlockChunkLoader;
import com.vinogratis.metlabmod.common.blocks.BlockDegenerator;
import com.vinogratis.metlabmod.common.blocks.BlockGenerator;
import com.vinogratis.metlabmod.common.blocks.BlockKiller;
import com.vinogratis.metlabmod.common.blocks.BlockLevitator;
import com.vinogratis.metlabmod.common.blocks.BlockMagnet;
import com.vinogratis.metlabmod.common.blocks.BlockMetLabFurnace;
import com.vinogratis.metlabmod.common.blocks.BlockMobBarrier;
import com.vinogratis.metlabmod.common.blocks.BlockNormalGenerator;
import com.vinogratis.metlabmod.common.blocks.BlockOculus;
import com.vinogratis.metlabmod.common.blocks.BlockOven;
import com.vinogratis.metlabmod.common.blocks.BlockProKiller;
import com.vinogratis.metlabmod.common.blocks.BlockQuarry;
import com.vinogratis.metlabmod.common.blocks.BlockRepairer;
import com.vinogratis.metlabmod.common.blocks.BlockStorage;
import com.vinogratis.metlabmod.common.blocks.MetBlock;
import com.vinogratis.metlabmod.common.items.ItemOculus;
import com.vinogratis.metlabmod.common.items.MetChest;
import com.vinogratis.metlabmod.common.items.MetItem;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

import static com.MetLabMod.metLabTabs;

public class MetLabBlocks
{
    public static Block reactor;
    public static Block normalgenerator;
    public static Block degenerator;
    public static Block reactoranimation;
    public static Block repairer;
    public static Block quarry;
    public static Block killer;
    public static Block levitator;
    public static Block barrier;
    public static Block oculus_block;
    public static Block magnet;
    public static Block generator;
    public static Block storage;
    public static Block metLabFurnace;
    public static Block oven;
    public static Block proKiller;
    public static Block chunkLoader;

    public static void preInit()
    {
        reactor = new MetBlock(Material.wood, "Reactor", "firstreactor").setCreativeTab(metLabTabs).setHardness(2.0f);
        reactoranimation = new MetBlock(Material.wood, "ReactorAnim", "firstreactoranimation").setCreativeTab(metLabTabs);
        repairer = new BlockRepairer();
        quarry = new BlockQuarry();
        killer = new BlockKiller();
        levitator = new BlockLevitator();
        barrier = new BlockMobBarrier();
        oculus_block = new BlockOculus();
        magnet = new BlockMagnet();
        generator = new BlockGenerator();
        degenerator = new BlockDegenerator("degenerator", Material.rock, 5.0F, 5.0F, Block.soundTypeStone);
        storage = new BlockStorage();
        normalgenerator = new BlockNormalGenerator();
        metLabFurnace = new BlockMetLabFurnace();
        oven = new BlockOven();
        proKiller = new BlockProKiller();
        chunkLoader = new BlockChunkLoader();

        GameRegistry.registerBlock(chunkLoader, "chunkLoader");
        GameRegistry.registerBlock(reactor, "reactor");
        GameRegistry.registerBlock(reactoranimation, "reactoranimation");
        GameRegistry.registerBlock(repairer, "repairer");
        GameRegistry.registerBlock(quarry, "quarry");
        GameRegistry.registerBlock(killer, "killer");
        GameRegistry.registerBlock(levitator, "levitator");
        GameRegistry.registerBlock(barrier, "barrier");
        GameRegistry.registerBlock(magnet, "magnet");
        GameRegistry.registerBlock(oculus_block, "oculus_block");
        GameRegistry.registerBlock(generator, "generator");
        GameRegistry.registerBlock(degenerator, "degenerator");
        GameRegistry.registerBlock(storage, "storage");
        GameRegistry.registerBlock(normalgenerator, "normalgenerator");
        GameRegistry.registerBlock(metLabFurnace, "metLAbFurnace");
        GameRegistry.registerBlock(oven, "oven");
        GameRegistry.registerBlock(proKiller, "proKiller");
    }
}
