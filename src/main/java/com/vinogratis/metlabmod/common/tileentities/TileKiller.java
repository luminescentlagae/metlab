package com.vinogratis.metlabmod.common.tileentities;

import net.minecraft.entity.monster.EntityMob;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;

import java.util.List;

public class TileKiller extends AbstractTile
{
    public float killerTickDamage = 10;
    public int killRadius = 4;

    @Override
    public void updateEntity()
    {
        if (ticksExisted % 15 == 0)
        {
            killAll();
        }

        super.updateEntity();
    }

    public void killAll()
    {
        List<EntityMob> entitiesToKill = getEntitiesToKill();

        for (EntityMob entityToKill : entitiesToKill)
        {
            entityToKill.attackEntityFrom(DamageSource.magic, killerTickDamage);
            entityToKill.addPotionEffect(new PotionEffect(Potion.jump.getId(), 55));
        }
    }

    private List<EntityMob> getEntitiesToKill()
    {
        List<EntityMob> result = worldObj.getEntitiesWithinAABB(EntityMob.class, AxisAlignedBB.getBoundingBox(
                xCoord - killRadius,
                yCoord - killRadius,
                zCoord - killRadius,
                xCoord + killRadius,
                yCoord + killRadius,
                zCoord + killRadius
        ));

        return result;
    }
}
