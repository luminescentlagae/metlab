package com.vinogratis.metlabmod.common.tileentities;

import com.MetLabMod;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

import java.util.List;

public class TileMagnet extends AbstractTile
{
    int range = 20;
    double speed = 4.5F;
    @Override

    public void updateEntity()
    {
        if (ticksExisted % 5 == 0)
        {
            magnet();
        }

        super.updateEntity();
    }

    public void magnet()
    {
        List<EntityItem> itemsToAttract = getItemsToAttract();

        for (EntityItem itemToAttract : itemsToAttract)
        {

            itemToAttract.motionX= (xCoord-itemToAttract.posX)/speed;
            itemToAttract.motionY= (yCoord-itemToAttract.posY+0.5)/speed;
            itemToAttract.motionZ= (zCoord-itemToAttract.posZ)/speed;

//            itemToAttract.addVelocity((xCoord - itemToAttract.posX) * range, (yCoord - itemToAttract.posY) * range, (zCoord - itemToAttract.posZ) * range);
        }
    }
    public List<EntityItem> getItemsToAttract(){
        List<EntityItem> result = worldObj.getEntitiesWithinAABB(EntityItem.class, AxisAlignedBB.getBoundingBox(
                xCoord - range,
                yCoord - range,
                zCoord - range,
                xCoord + range,
                yCoord + range,
                zCoord + range
        ));

        return result;
    }
}