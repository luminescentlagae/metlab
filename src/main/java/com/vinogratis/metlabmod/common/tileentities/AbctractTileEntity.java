package com.vinogratis.metlabmod.common.tileentities;

import net.minecraft.block.Block;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;

public abstract class AbctractTileEntity extends TileEntity
{

    public int ticksExisted;
    //public World worldObj;

    public boolean crash = false;

    public AbctractTileEntity(int i)
    {
        //this.worldObj = this.getWorldObj();
    }

    public Block getBlock()
    {
        return this.getWorldObj().getBlock(xCoord, yCoord, zCoord);
    }

    public void onCustomUpdate()
    {
    }

    public void writeCustomNBT(NBTTagCompound tag)
    {
    }

    public void readCustomNBT(NBTTagCompound tag)
    {
    }

    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        this.writeCustomNBT(tag);
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        this.readCustomNBT(tag);
    }

    public void updateEntity()
    {
        super.updateEntity();

        if (worldObj == null)
        {
            this.worldObj = this.getWorldObj();
        }

        if (!crash)
            try
            {
                this.onCustomUpdate();
            }
            catch (Exception e)
            {
                crash = true;
                e.printStackTrace();
            }
        else
        {
            if (ticksExisted % 100 == 0)
            {
                crash = false;
            }
        }
        ticksExisted ++;
    }

    public void setAirBlock()
    {
        this.getWorldObj().setBlockToAir(this.xCoord, this.yCoord, this.zCoord);
    }

    public void upd()
    {
        this.updateContainingBlockInfo();
        if(!this.worldObj.isRemote) {
            this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
        }
    }

    public Packet getDescriptionPacket() {
        NBTTagCompound nbttagcompound = new NBTTagCompound();
        this.writeCustomNBT(nbttagcompound);
        return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, -999, nbttagcompound);
    }

    public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt) {
        super.onDataPacket(net, pkt);
        this.readCustomNBT(pkt.func_148857_g());
    }
}