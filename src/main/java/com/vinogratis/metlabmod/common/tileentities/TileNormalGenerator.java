package com.vinogratis.metlabmod.common.tileentities;

import com.vinogratis.metlabmod.common.energy.IPowerContainer;
import com.vinogratis.metlabmod.common.energy.IPowerGenerator;
import com.vinogratis.metlabmod.lib.MetLabUtilsBlocks;
import com.vinogratis.metlabmod.lib.MetLabUtilsPower;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;

public class TileNormalGenerator extends AbctractTileEntity implements IPowerGenerator, IPowerContainer
{
    public int generateVaule;
    public int generateDelay;
    public int charge;
    public int maxCharge;
    public int payPowerPerTick;
    public int cx, cy, cz;
    public static float genCoef = 10.0F;

    public TileNormalGenerator()
    {

        super(1);
        this.generateDelay = MathHelper.floor_float(5 * genCoef);
        this.generateVaule = 5;
        this.payPowerPerTick = 5;
        this.maxCharge = 400;

    }

    public void setContainerTile(TileEntity tile)
    {

        if (tile instanceof IPowerContainer)
        {

            this.cx = tile.xCoord;
            this.cy = tile.yCoord;
            this.cz = tile.zCoord;

        }

    }

    public void payPowerToContainer()
    {
        if (cx != -1 && cy != -1 && cz != -1)
        {

            TileEntity tile = MetLabUtilsBlocks.getBlockTileEntity(this.getWorldObj(), this.cx, this.cy, this.cz);

            if (tile != null && tile instanceof IPowerContainer)
            {
                if (this.charge >= this.payPowerPerTick)
                    MetLabUtilsPower.payPowerFromTo(this, tile, this.payPowerPerTick);

            }

        }

    }

    public void tryToGenerate()
    {

        if (this.canGenerate())
        {
            if (this.ticksExisted % this.generateDelay == 0)
                this.charge += this.generateVaule;

        }

        this.payPowerToContainer();

    }

    public boolean canGenerate()
    {
        if (this.maxCharge >= this.charge + this.generateVaule)
            return true;

        return false;

    }

    public void onCustomUpdate()
    {

        this.tryToGenerate();

    }

    public void writeCustomNBT(NBTTagCompound tag)
    {

        tag.setInteger("cx", this.cx);
        tag.setInteger("cy", this.cy);
        tag.setInteger("cz", this.cz);

        tag.setInteger("charge", this.charge);

    }

    public void readCustomNBT(NBTTagCompound tag)
    {
        this.cx = tag.getInteger("cx");
        this.cy = tag.getInteger("cy");
        this.cz = tag.getInteger("cz");

        this.charge = tag.getInteger("charge");
    }

    @Override
    public int getCharge()
    {
        return this.charge;
    }

    @Override
    public void setCharge(int mana)
    {
        this.charge = mana;
    }

    @Override
    public int getMaxCharge()
    {
        return this.maxCharge;
    }

    @Override
    public void setMaxCharge(int mana)
    {
        this.maxCharge = mana;
    }

    @Override
    public boolean canPayPower()
    {
        return true;
    }

    @Override
    public int getGeneratePerTick()
    {
        return this.generateVaule;
    }

    @Override
    public int getGenerateDelay()
    {

        return this.generateDelay;

    }
}