package com.vinogratis.metlabmod.common.tileentities;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;

public class TileFuckThisPower extends AbstractInventoryTile
{
    public int power;
    public double powerFull;
    public String energyTagName = "Energy";

    public TileFuckThisPower(int inventorySize)
    {
        super(inventorySize);
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        if(tag.hasKey(energyTagName)){
            this.power = (int) tag.getDouble(energyTagName);
        }
    }

    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        NBTTagList energy = new NBTTagList();
        tag.setDouble(energyTagName, this.power);

    }

    public boolean isUseableByPlayer(EntityPlayer player)
    {
        return true;
    }
}