package com.vinogratis.metlabmod.common.tileentities;

import lombok.extern.log4j.Log4j2;
import lombok.var;

@Log4j2
public class TileStorage extends TileFuckThisPower
{
    public int genSearchRadius = 1;
    public TileGenerator linkedGenerator = null;

    public TileStorage()
    {
        super(1);
        // Максимальный заряд?
        this.powerFull = 5000;
    }

    @Override
    public void updateEntity()
    {
        updateGeneratorIfNeeded();
        super.updateEntity();
    }

    /**
     * Обновить генератор, если необходимо. <br>
     * Проверяет актуальность {@link #linkedGenerator} и ищет его в радиусе {@link #genSearchRadius} от тайла <br>
     *
     */
    public void updateGeneratorIfNeeded()
    {
        var worldObj = getWorldObj();

        if (isNeedsToUpdateGeneratorTile())
        {
            // Ищем тайл, который нам подходит. Для этого перебираем все позиции в радиусе поиска и ищем на них подходящие тайлы.

            for (var x = xCoord - genSearchRadius; x <= xCoord + genSearchRadius; x ++)
            {
                for (var y = yCoord - genSearchRadius; y <= yCoord + genSearchRadius; y ++)
                {
                    for (var z = zCoord - genSearchRadius; z <= zCoord + genSearchRadius; z ++)
                    {
                        var tile = worldObj.getTileEntity(x, y, z);

                        // Если тайл не null и его класс наследует или является TileGenerator, то он нам подходит
                        if (tile instanceof TileGenerator)
                        {
                            log.info("Found valid tile generator at {} {} {}", x, y, z);
                            linkedGenerator = (TileGenerator) tile;
                            break;
                        }
                    }
                }
            }
        }
    }

    /**
     * Необходимо ли обновить генератор, из которого забирается энергия? <br>
     * Возвращает <code>true</code>, если {@link #linkedGenerator} равен <code>null</code> или на его позиции уже находится другой тайл
     */
    public boolean isNeedsToUpdateGeneratorTile()
    {
        var worldObj = getWorldObj();

        if (linkedGenerator == null)
            return true;

        if (worldObj.getTileEntity(linkedGenerator.xCoord, linkedGenerator.yCoord, linkedGenerator.zCoord) != linkedGenerator)
            return true;

        return false;
    }
}
