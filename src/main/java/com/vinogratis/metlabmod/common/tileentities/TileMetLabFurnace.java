package com.vinogratis.metlabmod.common.tileentities;

import com.vinogratis.metlabmod.common.energy.IPowerContainer;
import com.vinogratis.metlabmod.config.MetLabFurnaceRecipes;
import com.vinogratis.metlabmod.lib.MetLabUtils;
import com.vinogratis.metlabmod.lib.MetLabUtilsInventory;
import com.vinogratis.metlabmod.lib.MetLabUtilsNBT;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.nbt.NBTTagCompound;

import static net.minecraft.tileentity.TileEntityFurnace.getItemBurnTime;

public class TileMetLabFurnace extends AbctractTileEntity implements IPowerContainer, ISidedInventory
{
    public int charge, maxCharge, accept, acceptDelay, tickCost;

    public int furnaceCookTime;

    public int burnTime, maxBurnTime;

    private ItemStack[] furnaceItemStacks = new ItemStack[3];
    public static final int slot_Input = 0, slot_Result = 1;

    public static final int[] accesInput = new int[]{slot_Input};
    public static final int[] accesResult = new int[]{slot_Result};

    public static final String nbt_InventoryPrefix = "FurnaceInventory";

    public boolean burning = false;

    public int rotation = 0;

    public TileMetLabFurnace()
    {
        super(1);

        this.maxCharge = 1000;
        this.accept = 10;
        this.acceptDelay = 10;
        this.charge = 0;
        this.tickCost = 1;
        this.maxBurnTime = 60;
        this.burnTime = maxBurnTime;
    }

    public void onCustomUpdate()
    {
        charge();
        this.burning = burn();
        //if (this.ticksExisted % 20 == 0) getWorldObj().markBlockForRenderUpdate(xCoord, yCoord, zCoord);
        if (this.ticksExisted % 20 == 0) this.markDirty();
    }

    public boolean burn()
    {
        if (getCharge() >= tickCost)
        {
            if (this.furnaceItemStacks[slot_Input] != null)
            {
                ItemStack result = FurnaceRecipes.smelting().getSmeltingResult(this.furnaceItemStacks[slot_Input]);

                if (result != null)
                {
                    if (this.canAddResult(result))
                    {
                        this.setCharge(this.getCharge() - tickCost);

                        this.burnTime --;


                        if (this.burnTime == 0)
                        {
                            if (!getWorldObj().isRemote)
                                addResult();

                            this.burnTime = maxBurnTime;
                        }

                        return true;

                    }
                }
            }
        }

        return false;
    }

    public void charge()
    {
        if (this.ticksExisted > 0 && this.ticksExisted % acceptDelay == 0)
        {
            if (this.getCharge() < this.getMaxCharge())
            {
                if (this.getCharge() + accept <= this.getMaxCharge())
                {
                    MetLabUtils.powerUtils.tryToAddCharge(this, accept);
                }
                else
                {
                    MetLabUtils.powerUtils.tryToAddCharge(this, this.getMaxCharge() - this.getCharge());
                }
            }
        }
    }

    public boolean hasCharge()
    {
        return this.getCharge() > 0;
    }

    @Override
    public int getCharge()
    {
        return charge;
    }

    @Override
    public void setCharge(int power)
    {
        charge = power;
    }

    @Override
    public int getMaxCharge()
    {
        return maxCharge;
    }

    @Override
    public void setMaxCharge(int power)
    {
        maxCharge = power;
    }

    @Override
    public boolean canPayPower()
    {
        return false;
    }

    public boolean canAddResult(ItemStack stack)
    {
        if (stack == null) return false;

        if (this.furnaceItemStacks[slot_Result] == null) return true;

        if (this.furnaceItemStacks[slot_Result].getItemDamage() != stack.getItemDamage()) return false;

        if (this.furnaceItemStacks[slot_Result].hasTagCompound() && stack.hasTagCompound() && this.furnaceItemStacks[slot_Result].getTagCompound().equals(stack.getTagCompound())) return false;

        if (this.furnaceItemStacks[slot_Result].hasTagCompound() && !stack.hasTagCompound()) return false;

        if (!this.furnaceItemStacks[slot_Result].hasTagCompound() && stack.hasTagCompound()) return false;

        if ( !(this.furnaceItemStacks[slot_Result].getItem() == stack.getItem() && this.furnaceItemStacks[slot_Result].stackSize + stack.stackSize <= 64) ) return false;

        return true;
    }

    public void addResult()
    {
        ItemStack stack = FurnaceRecipes.smelting().getSmeltingResult(this.furnaceItemStacks[slot_Input]).copy();

        if (stack == null) return;

        if (this.furnaceItemStacks[slot_Result] == null)
        {
            this.furnaceItemStacks[slot_Result] = stack;
            this.furnaceItemStacks[slot_Result].stackSize = stack.stackSize;
        }
        else this.furnaceItemStacks[slot_Result].stackSize += stack.stackSize;

        minusInput();

    }

    /** Возвращает True, если предмет заменился на null*/
    public boolean minusInput()
    {

        this.furnaceItemStacks[slot_Input].stackSize --;

        if (this.furnaceItemStacks[slot_Input].stackSize <= 0)
        {
            this.furnaceItemStacks[slot_Input] = null;

            return true;
        }

        return false;
    }

    @Override
    public void writeCustomNBT(NBTTagCompound tag)
    {

        tag.setInteger("accept", this.accept);
        tag.setInteger("acceptDelay", this.acceptDelay);
        tag.setInteger("burnTime", this.burnTime);
        tag.setInteger("maxBurnTime", this.maxBurnTime);
        tag.setInteger("maxCharge", this.maxCharge);
        tag.setInteger("charge", this.charge);
        tag.setInteger("tickCost", this.tickCost);
        tag.setInteger("rotation", this.rotation);

        writeInventory(tag);
    }

    public void addCharge(int power)
    {

        this.charge = this.charge + power;
        this.updateContainingBlockInfo();
        if(!this.worldObj.isRemote) {
            this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
        }

    }

    public void removeCharge(int power)
    {

        this.charge = this.charge - power;
        this.updateContainingBlockInfo();
        if(!this.worldObj.isRemote) {
            this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
        }
    }

    @Override
    public void readCustomNBT(NBTTagCompound tag)
    {
        if (tag.hasKey("accept"))
            this.accept = tag.getInteger("accept");
        if (tag.hasKey("acceptDelay"))
            this.acceptDelay = tag.getInteger("acceptDelay");
        if (tag.hasKey("burnTime"))
            this.burnTime = tag.getInteger("burnTime");
        if (tag.hasKey("maxBurnTime"))
            this.maxBurnTime = tag.getInteger("maxBurnTime");
        if (tag.hasKey("maxCharge"))
            this.maxCharge = tag.getInteger("maxCharge");
        if (tag.hasKey("charge"))
            this.charge = tag.getInteger("charge");
        if (tag.hasKey("ticks"))
            this.tickCost = tag.getInteger("tickCost");
        if (tag.hasKey("rotation"))
            this.rotation = tag.getInteger("rotation");

        readInventory(tag);
    }

    private boolean canSmelt() {
        if (this.furnaceItemStacks[0] == null) {
            return false;
        } else {
            ItemStack itemstack = MetLabFurnaceRecipes.smelting().getSmeltingResult(this.furnaceItemStacks[0]);
            if (itemstack == null) return false;
            if (this.furnaceItemStacks[2] == null) return true;
            if (!this.furnaceItemStacks[2].isItemEqual(itemstack)) return false;
            int result = furnaceItemStacks[2].stackSize + itemstack.stackSize;
            return result <= getInventoryStackLimit() && result <= this.furnaceItemStacks[2].getMaxStackSize();
        }
    }

    public void smeltItem() {
        if (this.canSmelt()) {
            ItemStack itemstack = MetLabFurnaceRecipes.smelting().getSmeltingResult(this.furnaceItemStacks[0]);

            if (this.furnaceItemStacks[2] == null) {
                this.furnaceItemStacks[2] = itemstack.copy();
            } else if (this.furnaceItemStacks[2].getItem() == itemstack.getItem()) {
                this.furnaceItemStacks[2].stackSize += itemstack.stackSize;
            }

            --this.furnaceItemStacks[0].stackSize;

            if(this.furnaceItemStacks[0].stackSize >= 0){
                this.furnaceItemStacks[0] = null;
            }
        }
    }

    @Override
    public void updateEntity()
    {
        boolean flag = this.burnTime > 0;
        boolean flag1 = false;

        if (this.burnTime > 0) {
            --this.burnTime;
        }

        if (!this.worldObj.isRemote) {
            if (this.burnTime == 0 && this.canSmelt()) {
                this.maxBurnTime = this.burnTime = getItemBurnTime(this.furnaceItemStacks[1]);

                if (this.burnTime > 0) {
                    flag1 = true;
                    if (this.furnaceItemStacks[1] != null) {
                        --this.furnaceItemStacks[1].stackSize;

                        if (this.furnaceItemStacks[1].stackSize == 0) {
                            this.furnaceItemStacks[1] = furnaceItemStacks[1].getItem().getContainerItem(this.furnaceItemStacks[1]);
                        }
                    }
                }
            }

            if (this.isBurning() && this.canSmelt()) {
                ++this.furnaceCookTime;
                if (this.furnaceCookTime == 200) {
                    this.furnaceCookTime = 0;
                    this.smeltItem();
                    flag1 = true;
                }
            } else {
                this.furnaceCookTime = 0;
            }
        }

    }

    public void writeInventory(NBTTagCompound tag)
    {
        MetLabUtilsNBT.writeInventoryToNBT(tag, furnaceItemStacks, nbt_InventoryPrefix);
    }

    public void readInventory(NBTTagCompound tag)
    {
        this.furnaceItemStacks = MetLabUtils.nbt.readInventoryFromNBT(tag, nbt_InventoryPrefix, getSizeInventory());
    }

    @Override
    public int[] getAccessibleSlotsFromSide(int side)
    {
        return side == 1 || side == 0 ? accesResult : accesInput;
    }

    @Override
    public boolean canInsertItem(int slot, ItemStack itemstack, int side)
    {
        return isItemValidForSlot(slot, itemstack);
    }

    @Override
    public boolean canExtractItem(int i, ItemStack itemstack, int j)
    {
        return true;
    }

    @Override
    public int getSizeInventory()
    {
        return this.furnaceItemStacks.length;
    }

    @Override
    public ItemStack getStackInSlot(int i)
    {
        return this.furnaceItemStacks[i];
    }

    @Override
    public ItemStack decrStackSize(int i, int j)
    {
        return MetLabUtilsInventory.decrStackSize(i, j, furnaceItemStacks);
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int i)
    {
        return MetLabUtils.inventory.getStackInSlotOnClosing(i, furnaceItemStacks);
    }

    @Override
    public void setInventorySlotContents(int i, ItemStack itemstack)
    {
        this.furnaceItemStacks[i] = itemstack;
    }

    @Override
    public String getInventoryName()
    {
        return "The Magic Furnace";
    }

    @Override
    public boolean hasCustomInventoryName()
    {
        return false;
    }

    @Override
    public int getInventoryStackLimit()
    {
        return 64;
    }

    @Override
    public boolean isUseableByPlayer(EntityPlayer entityplayer)
    {
        return true;
    }

    @Override
    public void openInventory(){}

    @Override
    public void closeInventory(){}

    @Override
    public boolean isItemValidForSlot(int i, ItemStack itemstack)
    {
        return true;
    }

    @SideOnly(Side.CLIENT)
    public int getCookProgressScaled(int par1) {
        return this.furnaceCookTime * par1 / 200;
    }

    @SideOnly(Side.CLIENT)
    public int getBurnTimeRemainingScaled(int par1) {
        if (this.maxBurnTime == 0) {
            this.maxBurnTime = 200;
        }

        return this.burnTime * par1 / this.maxBurnTime;
    }

    public boolean isBurning() {
        return this.charge > 0;
    }
}
