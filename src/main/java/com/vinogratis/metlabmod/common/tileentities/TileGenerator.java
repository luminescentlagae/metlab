package com.vinogratis.metlabmod.common.tileentities;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;

public class TileGenerator extends TileFuckThisPower
{
     String energyTagName = "Energy";

    public TileGenerator(double power, double powerFull)
    {
        super(1);
        this.power = (int) power;
        this.powerFull = powerFull;
    }


    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        if(tag.hasKey(energyTagName)){
            this.power = (int) tag.getDouble(energyTagName);
        }
    }

    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        NBTTagList energy = new NBTTagList();
        tag.setDouble(energyTagName, this.power);

    }

    public void updateEntity()
    {
        if (ticksExisted % 5 == 0)
        {
            for (int power1 = 0; power1 < powerFull; power1++)
            {
                power = power + 1;
                if (power == powerFull){
                    break;
                }
            }
        }

        super.updateEntity();
    }
}