package com.vinogratis.metlabmod.common.tileentities;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLiquid;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;

    public class TileQuarry extends AbstractTile
{
    public int digRadius = 4;

    @Override
    public void updateEntity()
    {
        if (ticksExisted % 15 == 0)
        {
            dig();
        }

        super.updateEntity();
    }

    public void dig()
    {
        for (int xPos = xCoord - digRadius; xPos <= xCoord + digRadius; xPos ++)
        {
            for (int zPos = zCoord - digRadius; zPos <= zCoord + digRadius; zPos ++)
            {
                for (int yPos = yCoord - 1; yPos >= 0; yPos --)
                {
                    Block blockToDig = worldObj.getBlock(xPos, yPos, zPos);

                    if (blockToDig != null)
                    {
                        if (isBlockAcceptsForDigging(blockToDig))
                        {
                            if (!worldObj.isRemote)
                            {
                                ArrayList<ItemStack> blockDrops = blockToDig.getDrops(worldObj, xPos, yPos, zPos, worldObj.getBlockMetadata(xPos, yPos, zPos), 0);
                                for (ItemStack dropItemStack : blockDrops)
                                {
                                    EntityItem entityItem = new EntityItem(worldObj, xCoord, yCoord + 3, zCoord, dropItemStack);
                                    worldObj.spawnEntityInWorld(entityItem);
                                }
                            }

                            worldObj.setBlock(xPos, yPos, zPos, Blocks.air);
                            break;
                        }
                    }
                }
            }
        }
    }

    private boolean isBlockAcceptsForDigging(Block blockToDig)
    {
        return blockToDig != Blocks.air && blockToDig != Blocks.bedrock && !(blockToDig instanceof BlockLiquid);
    }
}
