package com.vinogratis.metlabmod.common.tileentities;

import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.ChunkCoordinates;
import net.minecraft.util.DamageSource;

import java.util.List;

public class TileLevitator extends AbstractTile
{
    public int levitationRadius = 15;

    @Override
    public void updateEntity()
    {
        if (ticksExisted % 1 == 0)
        {
            levitation();
        }

        super.updateEntity();
    }

    public void levitation()
    {
        List<EntityPlayer> entitiesToLevitation = getEntitiesToLevitation();
        for (EntityPlayer entityToLevitation : entitiesToLevitation)
        {
            entityToLevitation.motionY = 0.2F;
            if(entityToLevitation.isSneaking()){
                entityToLevitation.motionY = -0.2F;
            }
        }
    }

    private List<EntityPlayer> getEntitiesToLevitation()
    {
        List<EntityPlayer> result = worldObj.getEntitiesWithinAABB(EntityPlayer.class, AxisAlignedBB.getBoundingBox(
                xCoord - 0.0F,
                yCoord - 0.0F,
                zCoord - 0.0F,
                xCoord + 1.0F,
                yCoord + levitationRadius,
                zCoord + 1.0F
        ));

        return result;
    }

}
