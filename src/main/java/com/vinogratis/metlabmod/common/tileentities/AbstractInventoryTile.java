package com.vinogratis.metlabmod.common.tileentities;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

import java.util.ArrayList;
import java.util.List;

public class AbstractInventoryTile extends AbstractTile implements IInventory
{
    public String inventoryTagName = "InventoryContents";
    public String inventoryName = "basic.inv";
    public List<ItemStack> inventoryStacks = new ArrayList<ItemStack>();

    public AbstractInventoryTile(int inventorySize)
    {
        for (int i1 = 0; i1 < inventorySize; i1 ++)
            inventoryStacks.add(null);
    }



    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        NBTTagList itemsList = tag.getTagList(inventoryTagName, 10);

        if (itemsList != null)
        {
            int itemTagsCount = itemsList.tagCount();

            for (int i1 = 0; i1 < itemTagsCount; i1 ++)
            {
                NBTTagCompound itemTagCompound = itemsList.getCompoundTagAt(i1);
                ItemStack stack = ItemStack.loadItemStackFromNBT(itemTagCompound);
                int slotId = itemTagCompound.getInteger("Slot");
                inventoryStacks.set(slotId, stack);
            }
        }

        super.readFromNBT(tag);
    }

    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        NBTTagList itemsList = new NBTTagList();

        for (int i1 = 0; i1 < inventoryStacks.size(); i1 ++)
        {
            ItemStack stack = inventoryStacks.get(i1);

            if (stack != null)
            {
                NBTTagCompound itemTagCompound = new NBTTagCompound();
                stack.writeToNBT(itemTagCompound);
                itemTagCompound.setInteger("Slot", i1);

                itemsList.appendTag(itemTagCompound);
            }
        }

        tag.setTag(inventoryTagName, itemsList);

        super.writeToNBT(tag);
    }

    @Override
    public int getSizeInventory()
    {
        return inventoryStacks.size();
    }


    @Override
    public ItemStack getStackInSlot(int slot)
    {
        return inventoryStacks.get(slot);
    }

    @Override
    public ItemStack decrStackSize(int slotId, int p_70298_2_)
    {
        if (this.inventoryStacks.get(slotId) != null)
        {
            ItemStack itemstack;

            if (this.inventoryStacks.get(slotId).stackSize <= p_70298_2_)
            {
                itemstack = this.inventoryStacks.get(slotId);
                this.inventoryStacks.set(slotId, null);
                this.markDirty();
                return itemstack;
            }
            else
            {
                itemstack = this.inventoryStacks.get(slotId).splitStack(p_70298_2_);

                if (this.inventoryStacks.get(slotId).stackSize == 0)
                {
                    this.inventoryStacks.set(slotId, null);
                }

                this.markDirty();
                return itemstack;
            }
        }
        else
        {
            return null;
        }
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int slot)
    {
        if (this.inventoryStacks.get(slot) != null)
        {
            ItemStack itemstack = this.inventoryStacks.get(slot);
            this.inventoryStacks.set(slot, null);
            return itemstack;
        }
        else
        {
            return null;
        }
    }

    @Override
    public void setInventorySlotContents(int slot, ItemStack stack)
    {
        inventoryStacks.set(slot, stack);
    }

    @Override
    public String getInventoryName()
    {
        return inventoryName;
    }

    @Override
    public boolean hasCustomInventoryName()
    {
        String invName = getInventoryName();

        return invName != null && !invName.isEmpty();
    }

    @Override
    public int getInventoryStackLimit()
    {
        return 64;
    }

    @Override
    public boolean isUseableByPlayer(EntityPlayer player)
    {
        return true;
    }

    @Override
    public void openInventory()
    {
        // Nope!
    }

    @Override
    public void closeInventory()
    {
        // Nope!
    }

    @Override
    public boolean isItemValidForSlot(int slot, ItemStack stack)
    {
        return true;
    }

}
