package com.vinogratis.metlabmod.common.tileentities;

import com.vinogratis.metlabmod.common.energy.IPowerContainer;
import com.vinogratis.metlabmod.common.energy.IPowerGenerator;
import com.vinogratis.metlabmod.lib.MetLabUtilsBlocks;
import com.vinogratis.metlabmod.lib.MetLabUtilsPower;
import lombok.var;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.inventory.IInventory;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;

import java.util.List;

public class TileProKiller extends AbstractInventoryTile implements IPowerContainer, IPowerGenerator, IInventory

{
    public int generateVaule;
    public int generateDelay;
    public int charge;
    public int maxCharge;
    public int payPowerPerTick;
    public int cx, cy, cz;

    public TileProKiller()
    {
        super(1);
        this.generateDelay = MathHelper.floor_float(5 * 10.0F);
        this.generateVaule = 5;
        this.payPowerPerTick = 5;
        this.maxCharge = 400;

    }

    public float killerTickDamage = 10;
    public int killRadius = 4;

    @Override
    public void updateEntity()
    {
        if (ticksExisted % 15 == 0)
        {
            killAll();
        }

        super.updateEntity();
    }

    public void onCustomUpdate()
    {
        dieAll();
        tryToGenerate();
    }

    public void dieAll()
    {
        List<EntityLivingBase> entitiesToDie = getEntitiesToDie();
        for (EntityLivingBase entity : entitiesToDie)
        {

            var entityLiving = (EntityLivingBase) entity;
            if (!entityLiving.isEntityAlive())
            {

                if (this.canGenerate())
                {
                    if (this.ticksExisted % this.generateDelay == 0)
                        this.charge += this.generateVaule;

                }
                continue;
            }

        }
    }

    List<EntityLivingBase> getEntitiesToDie()
    {
        List<EntityLivingBase> died = worldObj.getEntitiesWithinAABB(EntityLivingBase.class, AxisAlignedBB.getBoundingBox(xCoord - killRadius, yCoord - killRadius, zCoord - killRadius, xCoord + killRadius, yCoord + killRadius, zCoord + killRadius));

        return died;
    }
    //
    //    public void onDeath()
    //    {
    //        if (EntityLivingBase entity instanceof EntityLivingBase)
    //        {
    //            var entityLiving = (EntityLivingBase) entity;
    //            if (entityLiving.isEntityAlive())
    //            {
    //
    //            }
    //        }
    //    }

    public void killAll()
    {
        List<EntityMob> entitiesToKill = getEntitiesToKill();

        for (EntityMob entityToKill : entitiesToKill)
        {
            entityToKill.attackEntityFrom(DamageSource.magic, killerTickDamage);
        }
    }

    List<EntityMob> getEntitiesToKill()
    {
        List<EntityMob> result = worldObj.getEntitiesWithinAABB(EntityMob.class, AxisAlignedBB.getBoundingBox(xCoord - killRadius, yCoord - killRadius, zCoord - killRadius, xCoord + killRadius, yCoord + killRadius, zCoord + killRadius));

        return result;
    }

    public void tryToGenerate()
    {

        //        if (this.canGenerate())
        //        {
        //            if (this.ticksExisted % this.generateDelay == 0)
        //                this.charge += this.generateVaule;
        //
        //        }

        this.payPowerToContainer();

    }

    public void payPowerToContainer()
    {
        if (cx != -1 && cy != -1 && cz != -1)
        {

            TileEntity tile = MetLabUtilsBlocks.getBlockTileEntity(this.getWorldObj(), this.cx, this.cy, this.cz);

            if (tile != null && tile instanceof IPowerContainer)
            {
                if (this.charge >= this.payPowerPerTick)
                    MetLabUtilsPower.payPowerFromTo(this, tile, this.payPowerPerTick);

            }

        }

    }

    public void writeCustomNBT(NBTTagCompound tag)
    {

        tag.setInteger("cx", this.cx);
        tag.setInteger("cy", this.cy);
        tag.setInteger("cz", this.cz);

        tag.setInteger("charge", this.charge);

    }

    public void readCustomNBT(NBTTagCompound tag)
    {
        this.cx = tag.getInteger("cx");
        this.cy = tag.getInteger("cy");
        this.cz = tag.getInteger("cz");

        this.charge = tag.getInteger("charge");
    }

    public boolean canGenerate()
    {
        if (this.maxCharge >= this.charge + this.generateVaule)
            return true;

        return false;

    }

    @Override
    public int getCharge()
    {
        return this.charge;
    }

    @Override
    public void setCharge(int power)
    {
        this.charge = power;
    }

    @Override
    public int getMaxCharge()
    {
        return this.maxCharge;
    }

    @Override
    public void setMaxCharge(int power)
    {

        this.maxCharge = power;

    }

    @Override
    public boolean canPayPower()
    {
        return true;
    }

    @Override
    public int getGeneratePerTick()
    {
        return this.generateVaule;
    }

    @Override
    public int getGenerateDelay()
    {

        return this.generateDelay;

    }

}
