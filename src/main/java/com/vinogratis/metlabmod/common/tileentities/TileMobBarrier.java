package com.vinogratis.metlabmod.common.tileentities;

import net.minecraft.entity.monster.EntityMob;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;

import java.util.List;

public class TileMobBarrier extends AbstractTile
{
    public int barrierRadius = 20;

    @Override
    public void updateEntity()
    {
        if (ticksExisted % 1 == 0)
        {
            barrier();
        }
        super.updateEntity();
    }

    public void barrier(){
        List<EntityMob> entitiesToStop = getEntitiesToStop();

        for (EntityMob entityToStop : entitiesToStop)
        {
            entityToStop.motionY = 0.0F;
            entityToStop.motionX = 0.0F;
            entityToStop.motionZ = 0.0F;
        }
    }

    private List<EntityMob> getEntitiesToStop()
    {
        List<EntityMob> result = worldObj.getEntitiesWithinAABB(EntityMob.class, AxisAlignedBB.getBoundingBox(
                xCoord - 0.0F,
                yCoord - 0.0F,
                zCoord - 0.0F,
                xCoord + 1.0F,
                yCoord + barrierRadius,
                zCoord + 1.0F
        ));

        return result;
    }
}
