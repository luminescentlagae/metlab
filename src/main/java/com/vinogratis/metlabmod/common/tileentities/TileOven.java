package com.vinogratis.metlabmod.common.tileentities;

import com.vinogratis.metlabmod.common.energy.IPowerContainer;
import com.vinogratis.metlabmod.config.MetLabOvenRecipes;
import com.vinogratis.metlabmod.lib.MetLabUtils;
import com.vinogratis.metlabmod.lib.MetLabUtilsInventory;
import com.vinogratis.metlabmod.lib.MetLabUtilsNBT;
import com.vinogratis.metlabmod.lib.MetLabUtilsPower;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.nbt.NBTTagCompound;

public class TileOven extends AbctractTileEntity implements IPowerContainer, ISidedInventory
{

    public int charge, maxCharge, accept, acceptDelay, tickCost;

    public int burnTime, maxBurnTime;

    public ItemStack[] inventory = new ItemStack[2];
    public static final int slot_Input = 0, slot_Result = 1;

    public static final int[] accesInput = new int[] { slot_Input };
    public static final int[] accesResult = new int[] { slot_Result };

    public static final String nbt_InventoryPrefix = "FurnaceInventory";

    public boolean burning = false;

    public int rotation = 0;

    public TileOven()
    {
        super(1);

        this.maxCharge = 1000;
        this.accept = 10;
        this.acceptDelay = 10;
        this.charge = 0;
        this.tickCost = 1;
        this.maxBurnTime = 60;
        this.burnTime = maxBurnTime;
    }

    public void onCustomUpdate()
    {
        charge();
        this.burning = burn();
        //if (this.ticksExisted % 20 == 0) getWorldObj().markBlockForRenderUpdate(xCoord, yCoord, zCoord);
        if (this.ticksExisted % 20 == 0)
            this.markDirty();
    }

    public boolean burn()
    {
        if (getCharge() >= tickCost)
        {
            if (this.inventory[slot_Input] != null)
            {
                ItemStack result = MetLabOvenRecipes.smelting().getSmeltingResult(this.inventory[slot_Input]);

                if (result != null)
                {
                    if (this.canAddResult(result))
                    {
                        this.setCharge(this.getCharge() - tickCost);

                        this.burnTime--;

                        if (this.burnTime <= 0)
                        {
                            if (!getWorldObj().isRemote)
                                addResult();

                            this.burnTime = maxBurnTime;
                        }

                        return true;

                    }
                }
            }
        }

        return false;
    }

    public void charge()
    {
        if (this.ticksExisted > 0 && this.ticksExisted % acceptDelay == 0)
        {
            if (this.getCharge() < this.getMaxCharge())
            {
                if (this.getCharge() + accept <= this.getMaxCharge())
                {
                    MetLabUtilsPower.tryToAddCharge(this, accept);
                }
                else
                {
                    MetLabUtilsPower.tryToAddCharge(this, this.getMaxCharge() - this.getCharge());
                }
            }
        }
    }

    public boolean hasCharge()
    {
        return this.getCharge() > 0;
    }

    @Override
    public int getCharge()
    {
        return charge;
    }

    @Override
    public void setCharge(int power)
    {
        charge = power;
    }

    @Override
    public int getMaxCharge()
    {
        return maxCharge;
    }

    @Override
    public void setMaxCharge(int power)
    {
        maxCharge = power;
    }

    @Override
    public boolean canPayPower()
    {
        return false;
    }

    public boolean canAddResult(ItemStack stack)
    {
        if (stack == null)
            return false;

        if (this.inventory[slot_Result] == null)
            return true;

        if (this.inventory[slot_Result].getItemDamage() != stack.getItemDamage())
            return false;

        if (this.inventory[slot_Result].hasTagCompound() && stack.hasTagCompound() && this.inventory[slot_Result].getTagCompound().equals(stack.getTagCompound()))
            return false;

        if (this.inventory[slot_Result].hasTagCompound() && !stack.hasTagCompound())
            return false;

        if (!this.inventory[slot_Result].hasTagCompound() && stack.hasTagCompound())
            return false;

        if (!(this.inventory[slot_Result].getItem() == stack.getItem() && this.inventory[slot_Result].stackSize + stack.stackSize <= 64))
            return false;

        return true;
    }

    public void addResult()
    {
        ItemStack stack = MetLabOvenRecipes.smelting().getSmeltingResult(this.inventory[slot_Input]);

        if (stack == null)
            return;

        if (this.inventory[slot_Result] == null)
        {
            this.inventory[slot_Result] = stack;
            this.inventory[slot_Result].stackSize = stack.stackSize;
        }
        else
            this.inventory[slot_Result].stackSize += stack.stackSize;

        minusInput();

    }

    /**
     * Возвращает True, если предмет заменился на null
     */
    public boolean minusInput()
    {

        this.inventory[slot_Input].stackSize--;

        if (this.inventory[slot_Input].stackSize <= 0)
        {
            this.inventory[slot_Input] = null;

            return true;
        }

        return false;
    }

    @Override
    public void writeCustomNBT(NBTTagCompound tag)
    {
        tag.setInteger("accept", this.accept);
        tag.setInteger("acceptDelay", this.acceptDelay);
        tag.setInteger("burnTime", this.burnTime);
        tag.setInteger("maxBurnTime", this.maxBurnTime);
        tag.setInteger("maxCharge", this.maxCharge);
        tag.setInteger("charge", this.charge);
        tag.setInteger("tickCost", this.tickCost);
        tag.setInteger("rotation", this.rotation);

        writeInventory(tag);
    }

    @Override
    public void readCustomNBT(NBTTagCompound tag)
    {
        if (tag.hasKey("accept"))
            this.accept = tag.getInteger("accept");
        if (tag.hasKey("acceptDelay"))
            this.acceptDelay = tag.getInteger("acceptDelay");
        if (tag.hasKey("burnTime"))
            this.burnTime = tag.getInteger("burnTime");
        if (tag.hasKey("maxBurnTime"))
            this.maxBurnTime = tag.getInteger("maxBurnTime");
        if (tag.hasKey("maxCharge"))
            this.maxCharge = tag.getInteger("maxCharge");
        if (tag.hasKey("charge"))
            this.charge = tag.getInteger("charge");
        if (tag.hasKey("ticks"))
            this.tickCost = tag.getInteger("tickCost");
        if (tag.hasKey("rotation"))
            this.rotation = tag.getInteger("rotation");

        readInventory(tag);
    }

    public void writeInventory(NBTTagCompound tag)
    {
        MetLabUtilsNBT.writeInventoryToNBT(tag, inventory, nbt_InventoryPrefix);
    }

    public void readInventory(NBTTagCompound tag)
    {
        this.inventory = MetLabUtils.nbt.readInventoryFromNBT(tag, nbt_InventoryPrefix, getSizeInventory());
    }

    @Override
    public int[] getAccessibleSlotsFromSide(int side)
    {
        return side == 1 || side == 0 ? accesResult : accesInput;
    }

    @Override
    public boolean canInsertItem(int slot, ItemStack itemstack, int side)
    {
        return isItemValidForSlot(slot, itemstack);
    }

    @Override
    public boolean canExtractItem(int i, ItemStack itemstack, int j)
    {
        return true;
    }

    @Override
    public int getSizeInventory()
    {
        return this.inventory.length;
    }

    @Override
    public ItemStack getStackInSlot(int i)
    {
        return this.inventory[i];
    }

    @Override
    public ItemStack decrStackSize(int i, int j)
    {
        return MetLabUtilsInventory.decrStackSize(i, j, inventory);
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int i)
    {
        return MetLabUtils.inventory.getStackInSlotOnClosing(i, inventory);
    }

    @Override
    public void setInventorySlotContents(int i, ItemStack itemstack)
    {
        this.inventory[i] = itemstack;
    }

    @Override
    public String getInventoryName()
    {
        return "The Magic Furnace";
    }

    @Override
    public boolean hasCustomInventoryName()
    {
        return false;
    }

    @Override
    public int getInventoryStackLimit()
    {
        return 64;
    }

    @Override
    public boolean isUseableByPlayer(EntityPlayer entityplayer)
    {
        return true;
    }

    @Override
    public void openInventory()
    {
    }

    @Override
    public void closeInventory()
    {
    }

    @Override
    public boolean isItemValidForSlot(int i, ItemStack itemstack)
    {
        return true;
    }

}