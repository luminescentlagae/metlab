package com.vinogratis.metlabmod.common.tileentities;

import net.minecraft.tileentity.TileEntity;

public class AbstractTile extends TileEntity
{
    public int ticksExisted;

    @Override
    public void updateEntity()
    {
        ticksExisted ++;
        super.updateEntity();
    }
}
