package com.vinogratis.metlabmod.common.tileentities;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class TileRepairer extends AbstractInventoryTile
{
    public TileRepairer()
    {
        super(1);
    }

    @Override
    public void updateEntity()
    {
        if (ticksExisted % 40 == 0)
        {
            ItemStack stackToRepair = getStackInSlot(0);
            if (stackToRepair != null)
            {
                int itemToRepairDamage = stackToRepair.getItemDamage();

                if (itemToRepairDamage > 0)
                {
                    Item itemToRepair = stackToRepair.getItem();

                    if (itemToRepair.isRepairable())
                    {
                        stackToRepair.setItemDamage(Math.max(0, itemToRepairDamage - 10));
                    }
                }
            }
        }

        super.updateEntity();
    }

    @Override
    public int getInventoryStackLimit()
    {
        return 1;
    }
}
