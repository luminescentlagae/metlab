package com.vinogratis.metlabmod.common.items;

import net.minecraft.item.Item;

public class MetItem extends Item {

    public String texture = "";


    public MetItem()
    {
        super();
    }

    public MetItem setIIcon(String texture)
    {

        this.texture = texture;

        return this;

    }
}
