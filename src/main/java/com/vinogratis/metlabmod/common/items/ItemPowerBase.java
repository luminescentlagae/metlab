package com.vinogratis.metlabmod.common.items;

import com.vinogratis.metlabmod.common.energy.IPowerContainerItem;
import com.vinogratis.metlabmod.lib.MetLabUtils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import java.util.List;

public class ItemPowerBase extends MetItem implements IPowerContainerItem
{

    public int maxCharge;

    public boolean displayCharge = false;

    public ItemPowerBase(int mCharge)
    {
        super();
        this.maxCharge = mCharge;

    }

    public void checkNBT(ItemStack stack)
    {

        if (!stack.hasTagCompound())
        {

            NBTTagCompound tag = new NBTTagCompound();
            tag.setInteger("charge", 0);
            stack.setTagCompound(tag);

        }

    }

    @Override
    public int getCharge(ItemStack stack)
    {
        this.checkNBT(stack);

        return stack.getTagCompound().getInteger("charge");

    }

    @Override
    public int getMaxCharge()
    {
        return this.maxCharge;
    }

    @Override
    public void setCharge(ItemStack stack, int mana)
    {

        this.checkNBT(stack);
        stack.getTagCompound().setInteger("charge", mana);

    }

    @Override
    public void setMaxCharge(int mana)
    {

        this.maxCharge = mana;

    }

    @Override
    public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean par4)
    {
        if (displayCharge)
        {
            int charge = 0;

            try
            {
                charge = getCharge(stack);
            }
            catch (Exception e)
            {

            }

//            list.add(MetLabUtils.chat.getTransformedAndLocalized("(2)", "Charge:", "item.wand.charge") + " " + charge);
        }

        super.addInformation(stack, player, list, par4);
    }

}

