package com.vinogratis.metlabmod.common.items;

import com.MetLabMod;
import com.vinogratis.metlabmod.config.MetLabBlocks;
import net.minecraft.init.Blocks;

public class ItemOculus extends ItemSeedFood
{

    public ItemOculus()
    {
        super(1, 0.3F, MetLabBlocks.oculus_block, Blocks.farmland);
        setUnlocalizedName("oculus");
        setTextureName("metlabmod:oculus");
        setCreativeTab(MetLabMod.metLabTabs);
    }
}
