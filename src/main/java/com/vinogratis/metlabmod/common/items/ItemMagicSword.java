package com.vinogratis.metlabmod.common.items;

import com.MetLabMod;
import com.vinogratis.metlabmod.lib.MetLabToolMaterial;

public class ItemMagicSword extends ItemPowerSword
{

    public ItemMagicSword(MetLabToolMaterial mat)
    {
        super(mat);
        this.setMaxCharge(500);
        this.setCreativeTab(MetLabMod.metLabTabs);
        this.setMaxStackSize(1);
        this.setTicksRepair(30);
        this.setRepairCost(1);
    }
}
