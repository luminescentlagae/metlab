package com.vinogratis.metlabmod.common.items;

import com.MetLabMod;
import com.vinogratis.metlabmod.client.helper.ItemNBTHelper;
import com.vinogratis.metlabmod.client.helper.LibGuiIDs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.world.World;

public class MetChest extends Item
{
    private static final String TAG_ITEMS = "InvItems";
    private static final String TAG_SLOT = "Slot";

    public MetChest(String name, String texture, int maxStackSize) {
        this.setUnlocalizedName(name);
        this.setTextureName(MetLabMod.MODID + ":" + texture);
        this.setCreativeTab(MetLabMod.metLabTabs);
        this.maxStackSize = maxStackSize;
    }
    @Override
    public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
        player.openGui(MetLabMod.instance, LibGuiIDs.chestGuiId, world, 0, 0, 0);
        return stack;
    }


    public static ItemStack[] loadStacks(ItemStack stack) {
        NBTTagList var2 = ItemNBTHelper.getList(stack, TAG_ITEMS, 10, false);
        ItemStack[] inventorySlots = new ItemStack[36];
        for(int var3 = 0; var3 < var2.tagCount(); ++var3) {
            NBTTagCompound var4 = var2.getCompoundTagAt(var3);
            byte var5 = var4.getByte(TAG_SLOT);
            if(var5 >= 0 && var5 < inventorySlots.length)
                inventorySlots[var5] = ItemStack.loadItemStackFromNBT(var4);
        }

        return inventorySlots;
    }

    public static void setStacks(ItemStack stack, ItemStack[] inventorySlots) {
        NBTTagList var2 = new NBTTagList();
        for(int var3 = 0; var3 < inventorySlots.length; ++var3)
            if(inventorySlots[var3] != null) {
                NBTTagCompound var4 = new NBTTagCompound();
                var4.setByte(TAG_SLOT, (byte)var3);
                inventorySlots[var3].writeToNBT(var4);
                var2.appendTag(var4);
            }

        ItemNBTHelper.setList(stack, TAG_ITEMS, var2);
    }
}
