package com.vinogratis.metlabmod.common.items;

import com.vinogratis.metlabmod.lib.MetLabToolMaterial;
import com.vinogratis.metlabmod.lib.MetLabUtilsBlocks;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemPowerSword extends ItemPowerBase
{

    public int repairCost;
    public int ticksRepair;

    public static int maxCharge = 100;

    public int weaponDamage;
    public final MetLabToolMaterial toolMaterial;

    public ItemPowerSword(MetLabToolMaterial par2EnumToolMaterial)
    {
        super(200);
        this.toolMaterial = par2EnumToolMaterial;
        this.maxStackSize = 1;
        this.setMaxDamage(par2EnumToolMaterial.getMaxUses());
        this.weaponDamage = 4 + par2EnumToolMaterial.getDamageVsEntity();
    }

    public int func_82803_g()
    {
        return this.toolMaterial.getDamageVsEntity();
    }

    /**
     * Returns the strength of the stack against a given block. 1.0F base, (Quality+1)*2 if correct blocktype, 1.5F if
     * sword
     */
    public float getStrVsBlock(ItemStack par1ItemStack, Block par2Block)
    {
        if (par2Block == MetLabUtilsBlocks.web)
        {
            return 15.0F;
        }
        else
        {
            Material material = MetLabUtilsBlocks.getBlockMaterial(par2Block);
            return 1.0F;
        }
    }

    /**
     * Current implementations of this method in child classes do not use the entry argument beside ev. They just raise
     * the damage on the stack.
     */
    public boolean hitEntity(ItemStack par1ItemStack, EntityLivingBase par2EntityLivingBase, EntityLivingBase par3EntityLivingBase)
    {
        par1ItemStack.damageItem(1, par3EntityLivingBase);
        return true;
    }

    public boolean onBlockDestroyed(ItemStack par1ItemStack, World par2World, int par3, int par4, int par5, int par6, EntityLivingBase par7EntityLivingBase)
    {
        if (MetLabUtilsBlocks.getBlockById(par3).getBlockHardness(par2World, par4, par5, par6) != 0.0D)
        {
            par1ItemStack.damageItem(2, par7EntityLivingBase);
        }

        return true;
    }

    /**
     * Returns the damage against a given entity.
     */
    public int getDamageVsEntity(Entity par1Entity)
    {
        return this.weaponDamage;
    }

    @SideOnly(Side.CLIENT)

    /**
     * Returns True is the item is renderer in full 3D when hold.
     */
    public boolean isFull3D()
    {
        return true;
    }

    /**
     * returns the action that specifies what animation to play when the items is being used
     */
    public EnumAction getItemUseAction(ItemStack par1ItemStack)
    {
        return EnumAction.block;
    }

    /**
     * How long it takes to use or consume an item
     */
    public int getMaxItemUseDuration(ItemStack par1ItemStack)
    {
        return 72000;
    }

    /**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        par3EntityPlayer.setItemInUse(par1ItemStack, this.getMaxItemUseDuration(par1ItemStack));
        return par1ItemStack;
    }

    /**
     * Returns if the item (tool) can harvest results from the block type.
     */
    public boolean canHarvestBlock(Block par1Block)
    {
        return par1Block == MetLabUtilsBlocks.web;
    }

    /**
     * Return the enchantability factor of the item, most of the time is based on material.
     */
    public int getItemEnchantability()
    {
        return this.toolMaterial.getEnchantability();
    }

    /**
     * Return the name for this tool's material.
     */
    public String getToolMaterialName()
    {
        return this.toolMaterial.toString();
    }

    /**
     * Return whether this item is repairable in an anvil.
     */
    public boolean getIsRepairable(ItemStack par1ItemStack, ItemStack par2ItemStack)
    {
        return this.toolMaterial.customCraftingMaterial == par2ItemStack.getItem() ? true : super.getIsRepairable(par1ItemStack, par2ItemStack);
    }

    @Override
    public void onUpdate(ItemStack stack, World world, Entity ent, int par4, boolean par5)
    {

        if (ent instanceof EntityPlayer)
        {
            EntityPlayer player = (EntityPlayer)ent;

            if (player.ticksExisted % this.ticksRepair == 0)
            {

                if (stack.getItemDamage() > 0)
                {

                    if (this.getCharge(stack) >= this.repairCost)
                    {

                        this.setCharge(stack, this.getCharge(stack) - this.repairCost);
                        if (stack.getItemDamage() - 3 >= 0)
                            stack.setItemDamage(0);
                        else stack.setItemDamage(stack.getItemDamage() - 3);

                    }

                }

            }
        }

        super.onUpdate(stack, world, ent, par4, par5);
    }

    public void setTicksRepair(int ticks)
    {

        this.ticksRepair = ticks;

    }

    public void setRepairCost(int mana)
    {

        this.repairCost = mana;

    }
}