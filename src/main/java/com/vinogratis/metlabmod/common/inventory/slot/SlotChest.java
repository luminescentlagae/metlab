package com.vinogratis.metlabmod.common.inventory.slot;

import com.vinogratis.metlabmod.common.inventory.InventoryChest;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotChest extends Slot
{
    InventoryChest inv;

    public SlotChest(IInventory p_i1824_1_, int p_i1824_2_, int p_i1824_3_, int p_i1824_4_)
    {
        super(p_i1824_1_, p_i1824_2_, p_i1824_3_, p_i1824_4_);
    }

    @Override
    public void onSlotChange(ItemStack oldStack, ItemStack newStack) {
        inv.setInventorySlotContents(slotNumber, newStack);
    }

    @Override
    public boolean isItemValid(ItemStack stack) {
        return true;
    }
}
