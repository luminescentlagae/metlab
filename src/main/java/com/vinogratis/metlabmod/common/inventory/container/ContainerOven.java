package com.vinogratis.metlabmod.common.inventory.container;

import com.vinogratis.metlabmod.common.tileentities.TileOven;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnace;
import net.minecraft.item.ItemStack;

public class ContainerOven extends Container
{
    public ContainerOven(EntityPlayer player, IInventory tile)
    {
        int allYOffset = 0;
        int playerOffset = 0;

        // Слот с Инпутом
        this.addSlotToContainer(new Slot(tile, TileOven.slot_Input, 50, 30));

        // Слот с Результатом
        this.addSlotToContainer(new SlotFurnace(player, tile, TileOven.slot_Result, 110, 30));

        // Инвентарь игрока
        for (int i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 9; ++j)
            {
                this.addSlotToContainer(new Slot(player.inventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18 + allYOffset + playerOffset));
            }
        }

        for (int i = 0; i < 9; ++i)
        {
            this.addSlotToContainer(new Slot(player.inventory, i, 8 + i * 18, 142 + allYOffset + playerOffset));
        }

    }

    @Override
    public boolean canInteractWith(EntityPlayer entityplayer)
    {
        return true;
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int par2)
    {
        return null;
    }

}
