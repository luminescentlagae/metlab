package com.vinogratis.metlabmod.common.inventory.container;

import com.vinogratis.metlabmod.common.blocks.AbstractBlock;
import com.vinogratis.metlabmod.lib.MetLabUtilsBlocks;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public abstract class AbstractBlockContainer extends AbstractBlock implements ITileEntityProvider
{
    protected AbstractBlockContainer(Material par2Material)
    {
        super(par2Material);
        this.isBlockContainer = true;
    }

    /**
     * Called whenever the block is added into the world. Args: world, x, y, z
     */
    public void onBlockAdded(World par1World, int par2, int par3, int par4)
    {
        super.onBlockAdded(par1World, par2, par3, par4);
    }

    /**
     * ejects contained items into the world, and notifies neighbours of an update, as appropriate
     */
    public void breakBlock(World par1World, int par2, int par3, int par4, int par5, int par6)
    {
        //super.breakBlock(par1World, par2, par3, par4, par5, par6);
        MetLabUtilsBlocks.removeBlockTileEntity(par1World, par2, par3, par4);
    }

    public void breakBlock(World world, int x, int y, int z, Block block, int hz)
    {
        breakBlock(world, x, y, z, 0, 0);
        super.breakBlock(world, x, y, z, block, 0);
    }

    /**
     * Called when the block receives a BlockEvent - see World.addBlockEvent. By default, passes it on to the tile
     * entity at this location. Args: world, x, y, z, blockID, EventID, event parameter
     */
    public boolean onBlockEventReceived(World par1World, int par2, int par3, int par4, int par5, int par6)
    {
        super.onBlockEventReceived(par1World, par2, par3, par4, par5, par6);
        TileEntity tileentity = MetLabUtilsBlocks.getBlockTileEntity(par1World, par2, par3, par4);
        return tileentity != null ? tileentity.receiveClientEvent(par5, par6) : false;
    }

    public TileEntity createNewTileEntity(World world)
    {
        return null;
    }

    @Override
    public TileEntity createNewTileEntity(World world, int meta)
    {
        return createNewTileEntity(world);
    }
}

