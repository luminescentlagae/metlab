package com.vinogratis.metlabmod.common.inventory.container;

import com.vinogratis.metlabmod.common.inventory.slot.SlotRepairable;
import com.vinogratis.metlabmod.common.tileentities.TileRepairer;
import net.minecraft.entity.player.EntityPlayer;

public class ContainerRepairer extends AbstractContainer
{
    public ContainerRepairer(EntityPlayer player, TileRepairer tileRepairer)
    {
        this.playerInventoryYOffset = -30;
        addSlotToContainer(new SlotRepairable(tileRepairer, 0, 50, 20));


        addPlayerInventorySlots(player.inventory);
    }
}
