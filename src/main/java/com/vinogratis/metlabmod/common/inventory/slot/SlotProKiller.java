package com.vinogratis.metlabmod.common.inventory.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotProKiller extends Slot
{
    public SlotProKiller(IInventory inventory, int slotIndex, int slotX, int slotY)
    {
        super(inventory, slotIndex, slotX, slotY);
    }

    @Override
    public boolean isItemValid(ItemStack stack)
    {
        if (stack != null && stack.getItem() != null)
        {
            return stack.getItem().isRepairable();
        }

        return super.isItemValid(stack);
    }
}
