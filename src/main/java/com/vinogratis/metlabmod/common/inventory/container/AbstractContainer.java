package com.vinogratis.metlabmod.common.inventory.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class AbstractContainer extends Container
{
    public int playerInventoryYOffset = -10;

    public void addPlayerInventorySlots(InventoryPlayer playerInventory)
    {
        int yOffset = playerInventoryYOffset;

        for (int verticalRow = 0; verticalRow < 3; ++verticalRow)
        {
            for (int horizontalSlot = 0; horizontalSlot < 9; ++horizontalSlot)
            {
                this.addSlotToContainer(new Slot(playerInventory, horizontalSlot + verticalRow * 9 + 9, 8 + horizontalSlot * 18, 103 + verticalRow * 18 + yOffset));
            }
        }

        for (int horizontalSlot = 0; horizontalSlot < 9; ++horizontalSlot)
        {
            this.addSlotToContainer(new Slot(playerInventory, horizontalSlot, 8 + horizontalSlot * 18, 161 + yOffset));
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer player)
    {
        return true;
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slot)
    {
        return null;
    }
}
