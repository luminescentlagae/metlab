package com.vinogratis.metlabmod.common.inventory.container;

import com.vinogratis.metlabmod.common.inventory.slot.SlotProKiller;
import com.vinogratis.metlabmod.common.tileentities.TileProKiller;
import net.minecraft.entity.player.EntityPlayer;

public class ContainerProKiller extends AbstractContainer
{
    public ContainerProKiller(EntityPlayer player, TileProKiller tileProKiller)
    {
        this.playerInventoryYOffset = -30;
        addSlotToContainer(new SlotProKiller(tileProKiller, 0, 50, 20));


        addPlayerInventorySlots(player.inventory);
    }
}
