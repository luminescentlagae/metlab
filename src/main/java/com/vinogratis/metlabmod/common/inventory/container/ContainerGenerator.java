package com.vinogratis.metlabmod.common.inventory.container;

import com.vinogratis.metlabmod.common.tileentities.TileGenerator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class ContainerGenerator extends AbstractContainer
{
    public ContainerGenerator(EntityPlayer player, TileGenerator tileGenerator)
    {
        this.playerInventoryYOffset = -30;

        addPlayerInventorySlots(player.inventory);
    }

    @Override
    public boolean canInteractWith(EntityPlayer entityplayer)
    {
        return true;
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int par2)
    {
        return null;
    }
}
