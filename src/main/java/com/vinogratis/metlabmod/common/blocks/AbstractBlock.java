package com.vinogratis.metlabmod.common.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class AbstractBlock extends Block
{
    public String texture = "";
    public boolean isGlass = false;
    public int renderBlockPass = 0;

    public AbstractBlock(Material par2Material)
    {
        super(par2Material);
        this.setHardness(2.0F);
    }

    public Block setTexture(String texture)
    {
        this.texture = texture;
        return this;
    }

    public boolean isOpaqueCube()
    {
        return !isGlass;
    }

    public boolean renderAsNormalBlock()
    {
        return !isGlass;
    }

    public boolean isNormalCube()
    {
        return !isGlass;
    }



    public void registerIcons(IIconRegister par1IIconRegister)
    {
        if (this.texture.equals("")) this.blockIcon = par1IIconRegister.registerIcon(this.getUnlocalizedName());
        else this.blockIcon = par1IIconRegister.registerIcon("metlabmod:"+this.texture);
    }

    @Override
    public void registerBlockIcons(IIconRegister ir)
    {
        registerIcons(ir);
    }

    @Override
    public IIcon getIcon(int p_149691_1_, int p_149691_2_)
    {
        return this.blockIcon;
    }

    @Override
    public int getRenderBlockPass()
    {
        return renderBlockPass;
    }

    public void onNeighborBlockChange(World world, int x, int y, int z, int meta)
    {

    }

    @Override
    public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
    {
        this.onNeighborBlockChange(world, x, y, z, 0);
    }
}
