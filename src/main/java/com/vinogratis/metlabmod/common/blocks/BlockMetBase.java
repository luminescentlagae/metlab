package com.vinogratis.metlabmod.common.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockMetBase extends Block
{

    public BlockMetBase(String name, Material material, float hardness, float resistanse, SoundType soundType) {

        super(material);

        this.setBlockName(name);
        this.setHardness(hardness);
        this.setResistance(resistanse);
        this.setStepSound(soundType);
    }
}