package com.vinogratis.metlabmod.common.blocks;

import com.MetLabMod;
import com.vinogratis.metlabmod.common.tileentities.TileKiller;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockKiller extends BlockContainer
{
    public BlockKiller()
    {
        super(Material.iron);
        setCreativeTab(MetLabMod.metLabTabs);
        setBlockName("killer");
        setBlockTextureName(MetLabMod.MODID + ":");
    }

    @Override
    public TileEntity createNewTileEntity(World world, int meta)
    {
        return new TileKiller();
    }
}
