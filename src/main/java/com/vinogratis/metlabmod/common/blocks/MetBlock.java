package com.vinogratis.metlabmod.common.blocks;

import com.MetLabMod;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class MetBlock extends Block
{
    public MetBlock(Material p_i45394_1_, String name, String texture)
    {
        super(p_i45394_1_);
        this.setBlockName(name);
        this.setLightLevel(0F);
        this.setLightOpacity(10);
        this.setHardness(1.0F);
        this.setCreativeTab(MetLabMod.metLabTabs);
        this.setResistance(10.F);
        this.setHarvestLevel("axe", 3);
        this.setStepSound(soundTypeWood);
        //        this.setBlockUnbreakable()
        //        this.setBlockTextureName("metlabmod:firstreactor");
        this.setBlockTextureName(MetLabMod.MODID + ":" + texture);

    }

    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x, int y, int z)
    {
        return null;
    }

    @SideOnly(Side.CLIENT)
    public boolean onBlockActivated(World world, int p2, int p3, int p4, EntityPlayer player, int p6, float p7, float p8, float p9)
    {
        Minecraft.getMinecraft().thePlayer.sendChatMessage("brrrrrrrrr");
        return true;
    }

}

