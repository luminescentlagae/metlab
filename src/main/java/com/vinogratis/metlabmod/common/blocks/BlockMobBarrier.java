package com.vinogratis.metlabmod.common.blocks;

import com.MetLabMod;
import com.vinogratis.metlabmod.common.tileentities.TileLevitator;
import com.vinogratis.metlabmod.common.tileentities.TileMobBarrier;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockMobBarrier extends BlockContainer
{
    public BlockMobBarrier()
        {
            super(Material.iron);
            setCreativeTab(MetLabMod.metLabTabs);
            setBlockName("barrier");
            setBlockTextureName(MetLabMod.MODID + ":");
        }

        @Override
        public TileEntity createNewTileEntity(World world, int meta)
        {
            return new TileMobBarrier();
        }
}
