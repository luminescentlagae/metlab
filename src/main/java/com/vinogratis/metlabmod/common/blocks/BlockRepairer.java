package com.vinogratis.metlabmod.common.blocks;

import com.MetLabMod;
import com.vinogratis.metlabmod.client.helper.LibGuiIDs;
import com.vinogratis.metlabmod.common.tileentities.TileRepairer;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockRepairer extends BlockContainer
{
    public BlockRepairer()
    {
        super(Material.iron);
        setBlockTextureName(MetLabMod.MODID + ":repairer_side");
        setBlockName("repair_station");
        setCreativeTab(MetLabMod.metLabTabs);
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int meta, float hitZX, float hitY, float hitZ)
    {
        player.openGui(MetLabMod.instance, LibGuiIDs.repairerGuiId, world, x, y, z);
        return true;
    }

    @Override
    public TileEntity createNewTileEntity(World world, int metadata)
    {
        return new TileRepairer();
    }




}
