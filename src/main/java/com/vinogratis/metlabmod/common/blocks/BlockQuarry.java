package com.vinogratis.metlabmod.common.blocks;

import com.MetLabMod;
import com.vinogratis.metlabmod.common.tileentities.TileQuarry;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockQuarry extends BlockContainer
{
    public BlockQuarry()
    {
        super(Material.iron);
        setCreativeTab(MetLabMod.metLabTabs);
        setBlockName("quarry");
        setBlockTextureName(MetLabMod.MODID + ":quarry");
    }

    @Override
    public TileEntity createNewTileEntity(World world, int meta)
    {
        return new TileQuarry();
    }
}
