package com.vinogratis.metlabmod.common.blocks;

import com.MetLabMod;
import com.vinogratis.metlabmod.common.tileentities.TileDegenerator;
import com.vinogratis.metlabmod.common.tileentities.TileMetBlock;
import net.minecraft.block.Block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.world.World;

public class BlockDegenerator extends TileMetBlock<TileDegenerator>
    {

        public BlockDegenerator(String name, Material material, float hardness, float resistanse, SoundType soundType) {

            super(name, material, hardness, resistanse, soundType);

            this.setHarvestLevel("pickaxe", 3);
            this.setBlockName("degenerator_anton");
            this.setCreativeTab(MetLabMod.metLabTabs);
            this.setBlockTextureName(MetLabMod.MODID + ":" + "counter/counter");
        }

        @Override
        public boolean onBlockActivated(World world, int xPos, int yPos, int zPos, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {

            if (!world.isRemote) {

                TileDegenerator tileEntity = this.getTileEntity(world, xPos, yPos, zPos);

                switch (side) {

                case 0://Bottom
                    tileEntity.decrementCount();
                    break;

                case 1://Top
                    tileEntity.incrementCount();
                    break;

                default:
                    break;
                }

                player.addChatMessage(new ChatComponentTranslation("tile.counter.current " + tileEntity.getCount()));
            }

            return true;
        }

        @Override
        public Class<TileDegenerator> getTileEntityClass() {

            return TileDegenerator.class;
        }

        @Override
        public TileDegenerator createTileEntity(World world, int meta) {

            return new TileDegenerator();
        }
    }