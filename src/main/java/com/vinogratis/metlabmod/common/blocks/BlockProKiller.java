package com.vinogratis.metlabmod.common.blocks;

import com.MetLabMod;
import com.vinogratis.metlabmod.common.tileentities.TileProKiller;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockProKiller extends BlockContainer
{
    public BlockProKiller()
    {
        super(Material.iron);
        setCreativeTab(MetLabMod.metLabTabs);
        setBlockName("proKiller");
        setBlockTextureName(MetLabMod.MODID + ":");
    }

    @Override
    public TileEntity createNewTileEntity(World world, int meta)
    {
        return new TileProKiller();
    }
}
