package com.vinogratis.metlabmod.common.blocks;

import com.MetLabMod;
import com.vinogratis.metlabmod.client.helper.LibGuiIDs;
import com.vinogratis.metlabmod.common.tileentities.TileGenerator;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class BlockGenerator extends BlockContainer
{
    public BlockGenerator()
    {
        super(Material.iron);
        setCreativeTab(MetLabMod.metLabTabs);
        setBlockName("generator");
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9)
    {

        player.openGui(MetLabMod.instance, LibGuiIDs.generatorGuiId, world, x, y, z);

        return true;
    }

    @Override
    public TileGenerator createNewTileEntity(World world, int metadata)
    {
        return new TileGenerator(0, 500);
    }

}
