package com.vinogratis.metlabmod.common.blocks;

import com.MetLabMod;
import com.vinogratis.metlabmod.common.tileentities.TileStorage;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockStorage extends BlockContainer
{
    public BlockStorage()
    {
        super(Material.rock);
        setCreativeTab(MetLabMod.metLabTabs);
        setBlockName("met_storage");
    }

    @Override
    public TileEntity createNewTileEntity(World world, int meta)
    {
        return new TileStorage();
    }
}
