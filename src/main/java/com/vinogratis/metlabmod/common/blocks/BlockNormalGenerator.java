package com.vinogratis.metlabmod.common.blocks;

import com.MetLabMod;
import com.vinogratis.metlabmod.client.helper.LibGuiIDs;
import com.vinogratis.metlabmod.common.inventory.container.AbstractBlockContainer;
import com.vinogratis.metlabmod.common.tileentities.TileNormalGenerator;
import com.vinogratis.metlabmod.lib.MetLabUtilsBlocks;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockNormalGenerator extends AbstractBlockContainer{
        public BlockNormalGenerator()
        {
            super( Material.rock);
            this.setCreativeTab(MetLabMod.metLabTabs);
            this.setTexture("powerCollector");
            this.setHardness(MetLabUtilsBlocks.getBlockHardness(MetLabUtilsBlocks.stone));

        }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9)
    {

        player.openGui(MetLabMod.instance, LibGuiIDs.normalGeneratorGuiId, world, x, y, z);

        return true;
    }

    @Override
    public TileEntity createNewTileEntity(World world)
    {
        return new TileNormalGenerator();
    }
}