package com.vinogratis.metlabmod.common.blocks;

import com.MetLabMod;
import com.vinogratis.metlabmod.common.tileentities.TileKiller;
import com.vinogratis.metlabmod.common.tileentities.TileMagnet;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockMagnet extends BlockContainer
{
    public BlockMagnet()
    {
        super(Material.iron);
        setCreativeTab(MetLabMod.metLabTabs);
        setBlockName("magnet");
        setBlockTextureName(MetLabMod.MODID + ":");
    }

    @Override
    public TileEntity createNewTileEntity(World world, int meta)
    {
        return new TileMagnet();
    }
}