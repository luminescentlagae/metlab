package com.vinogratis.metlabmod.common.blocks;

import com.MetLabMod;
import com.vinogratis.metlabmod.client.helper.LibGuiIDs;
import com.vinogratis.metlabmod.common.inventory.container.AbstractBlockContainer;
import com.vinogratis.metlabmod.common.tileentities.TileOven;
import com.vinogratis.metlabmod.lib.MetLabUtils;
import com.vinogratis.metlabmod.lib.MetLabUtilsBlocks;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class BlockOven extends AbstractBlockContainer
{
    public BlockOven()
    {
        super(Material.rock);
        this.setCreativeTab(MetLabMod.metLabTabs);
        this.setHardness(MetLabUtilsBlocks.getBlockHardness(MetLabUtilsBlocks.furnaceBurning));
        setBlockName("Oven");
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9)
    {

        player.openGui(MetLabMod.instance, LibGuiIDs.oven, world, x, y, z);

        return true;
    }

    @Override
    public TileEntity createNewTileEntity(World world)
    {
        return new TileOven();
    }

    public void onBlockPlacedBy(World par1World, int par2, int par3, int par4, EntityLivingBase par5EntityLivingBaseBase, ItemStack par6ItemStack)
    {
        int l = MathHelper.floor_double((double) (par5EntityLivingBaseBase.rotationYaw * (float) 4 / 360.0F) + 0.5D) & 4 - 1;
        l %= 4;
        TileOven tile = (TileOven) MetLabUtilsBlocks.getBlockTileEntity(par1World, par2, par3, par4);
        tile.rotation = l;

        //EldrichMagic.logger.dev_Message(false, "<BlockMagicFurnace> Rotation is "+l);
    }


    @Override
    public void breakBlock(World world, int x, int y, int z, int par5, int par6)
    {
        MetLabUtils.inventory.dropItems(world, x, y, z);
        super.breakBlock(world, x, y, z, par5, par6);
    }
}