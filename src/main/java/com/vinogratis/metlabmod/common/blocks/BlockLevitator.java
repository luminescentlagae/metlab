package com.vinogratis.metlabmod.common.blocks;

import com.MetLabMod;
import com.vinogratis.metlabmod.common.tileentities.TileKiller;
import com.vinogratis.metlabmod.common.tileentities.TileLevitator;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockLevitator extends BlockContainer
{
    public BlockLevitator()
    {
        super(Material.iron);
        setCreativeTab(MetLabMod.metLabTabs);
        setBlockName("levitator");
        setBlockTextureName(MetLabMod.MODID + ":");
    }

    @Override
    public TileEntity createNewTileEntity(World world, int meta)
    {
        return new TileLevitator();
    }
}
