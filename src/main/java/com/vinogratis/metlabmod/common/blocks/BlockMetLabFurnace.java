package com.vinogratis.metlabmod.common.blocks;

import com.MetLabMod;
import com.vinogratis.metlabmod.common.inventory.container.AbstractBlockContainer;
import com.vinogratis.metlabmod.common.tileentities.TileMetLabFurnace;
import com.vinogratis.metlabmod.lib.MetLabUtilsBlocks;
import com.vinogratis.metlabmod.lib.MetLabUtilsInventory;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class BlockMetLabFurnace extends AbstractBlockContainer
{

//    public IIcon litIIcon, offIIcon, sideIIcon;
//    public String litIIconStr = "magicFurnace_lit", offIIconStr = "magicFurnace_off", sideIIconStr = "runicCrystalGen_off";

    public BlockMetLabFurnace()
    {
        super(Material.rock);
        this.setCreativeTab(MetLabMod.metLabTabs);
        this.setHardness(MetLabUtilsBlocks.getBlockHardness(MetLabUtilsBlocks.furnaceBurning));
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9)
    {

//        player.openGui(MetLabMod.instance, LibGuiIDs.metLabFurnace, world, x, y, z);

        return true;
    }

    @Override
    public TileEntity createNewTileEntity(World world)
    {
        return new TileMetLabFurnace();
    }

    public void onBlockPlacedBy(World par1World, int par2, int par3, int par4, EntityLivingBase par5EntityLivingBaseBase, ItemStack par6ItemStack)
    {
        int l = MathHelper.floor_double((double)(par5EntityLivingBaseBase.rotationYaw * (float)4 / 360.0F) + 0.5D) & 4 - 1;
        l %= 4;
        TileMetLabFurnace tile = (TileMetLabFurnace)MetLabUtilsBlocks.getBlockTileEntity(par1World, par2, par3, par4);
        tile.rotation = l;

        //EldrichMagic.logger.dev_Message(false, "<BlockMagicFurnace> Rotation is "+l);
    }

//    @Override
//    public void registerIcons(IIconRegister ir)
//    {
//        this.litIIcon = ir.registerIcon(getBlocksTextureFolder()+litIIconStr);
//        this.offIIcon = ir.registerIcon(getBlocksTextureFolder()+offIIconStr);
//        this.sideIIcon = ir.registerIcon(getBlocksTextureFolder()+sideIIconStr);
//    }

//    public IIcon getIcon(IBlockAccess world, int x, int y, int z, int meta)
//    {
//        return getBlockTexture(world, x, y, z, meta);
//    }

//    public IIcon getBlockTexture(IBlockAccess world, int x, int y, int z, int side)
//    {
//
//        TileMetLabFurnace tile = (TileMetLabFurnace)MetLabUtilsBlocks.getBlockTileEntity(world, x, y, z);
//
//        if (tile.rotation == 2) if (side == 3) return !tile.burning ? offIIcon : litIIcon;
//        if (tile.rotation == 3) if (side == 4) return !tile.burning ? offIIcon : litIIcon;
//        if (tile.rotation == 0) if (side == 2) return !tile.burning ? offIIcon : litIIcon;
//        if (tile.rotation == 1) if (side == 5) return !tile.burning ? offIIcon : litIIcon;
//
//        return sideIIcon;
//    }

//    @Override
//    public IIcon getIcon(int side, int meta)
//    {
//        if (side == 3) return offIIcon;
//
//        return sideIIcon;
//    }

    @Override
    public void breakBlock(World world, int x, int y, int z, int par5, int par6)
    {
        MetLabUtilsInventory.dropItems(world, x, y, z);
        super.breakBlock(world, x, y, z, par5, par6);
    }
}
