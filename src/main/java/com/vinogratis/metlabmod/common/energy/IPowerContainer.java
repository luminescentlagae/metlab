package com.vinogratis.metlabmod.common.energy;

public interface IPowerContainer
{
    int getCharge();

    void setCharge(int power);

    int getMaxCharge();

    void setMaxCharge(int power);

    boolean canPayPower();
}
