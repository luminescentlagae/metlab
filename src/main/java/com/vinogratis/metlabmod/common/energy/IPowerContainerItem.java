package com.vinogratis.metlabmod.common.energy;

import net.minecraft.item.ItemStack;

public interface IPowerContainerItem
{
    public int getCharge(ItemStack stack);
    public int getMaxCharge();
    public void setCharge(ItemStack stack, int power);
    public void setMaxCharge(int power);
}
