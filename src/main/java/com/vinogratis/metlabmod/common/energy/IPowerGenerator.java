package com.vinogratis.metlabmod.common.energy;

public interface IPowerGenerator
{

    public int getGeneratePerTick();

    int getGenerateDelay();

}
