package com.vinogratis.metlabmod.common.fuckingpowertools;

import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBuf;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import java.text.DecimalFormat;

public class DataSukaUtils
{
    public static DataSukaUtils instance = new DataSukaUtils();

    public void writeObjectToBytes(ByteBuf bytes, int dataType, Object object){
        switch (dataType){
        case FuckingFields.BYTE_ID:
            bytes.writeByte((Byte)object);
            break;
        case FuckingFields.SHORT_ID:
            bytes.writeShort((Short) object);
            break;
        case FuckingFields.INT_ID:
            bytes.writeInt((Integer) object);
            break;
        case FuckingFields.LONG_ID:
            bytes.writeLong((Long) object);
            break;
        case FuckingFields.FLOAT_ID:
            bytes.writeFloat((Float) object);
            break;
        case FuckingFields.DOUBLE_ID:
            bytes.writeDouble((Double) object);
            break;
        case FuckingFields.CHAR_ID:
            bytes.writeChar((Character) object);
            break;
        case FuckingFields.STRING_ID:
            ByteBufUtils.writeUTF8String(bytes, (String) object);
            break;
        case FuckingFields.BOOLEAN_ID:
            bytes.writeBoolean((Boolean) object);
            break;
        case FuckingFields.INT_PAIR_ID:
            bytes.writeInt(((IntPair)object).i1);
            bytes.writeInt(((IntPair)object).i2);
            break;
        }
    }

    public Object readObjectFromBytes(ByteBuf bytes, int dataType){
        switch (dataType){
        case FuckingFields.BYTE_ID:
            return bytes.readByte();
        case FuckingFields.SHORT_ID:
            return bytes.readShort();
        case FuckingFields.INT_ID:
            return bytes.readInt();
        case FuckingFields.LONG_ID:
            return bytes.readLong();
        case FuckingFields.FLOAT_ID:
            return bytes.readFloat();
        case FuckingFields.DOUBLE_ID:
            return bytes.readDouble();
        case FuckingFields.CHAR_ID:
            return bytes.readChar();
        case FuckingFields.STRING_ID:
            return ByteBufUtils.readUTF8String(bytes);
        case FuckingFields.BOOLEAN_ID:
            return bytes.readBoolean();
        case FuckingFields.INT_PAIR_ID:
            IntPair tx = new IntPair(0, 0);
            tx.i1 = bytes.readInt();
            tx.i2 = bytes.readInt();
            return tx;
        }
        return null;
    }

    public static void writeObjectToItem(ItemStack stack, Object value, int datatype, String name){
        switch (datatype){
        case FuckingFields.BYTE_ID:
            FuckingNbtHelper.setByte(stack, name, (Byte) value);
            break;
        case FuckingFields.SHORT_ID:
            FuckingNbtHelper.setShort(stack, name, (Short) value);
            break;
        case FuckingFields.INT_ID:
            FuckingNbtHelper.setInteger(stack, name, (Integer) value);
            break;
        case FuckingFields.LONG_ID:
            FuckingNbtHelper.setLong(stack, name, (Long) value);
            break;
        case FuckingFields.FLOAT_ID:
            FuckingNbtHelper.setFloat(stack, name, (Float) value);
            break;
        case FuckingFields.DOUBLE_ID:
            FuckingNbtHelper.setDouble(stack, name, (Double) value);
            break;
        //			case References.CHAR_ID:
        //				ItemNBTHelper.setChar(stack, value.name, (Byte)value.value);
        //				break;
        case FuckingFields.STRING_ID:
            FuckingNbtHelper.setString(stack, name, (String) value);
            break;
        case FuckingFields.BOOLEAN_ID:
            FuckingNbtHelper.setBoolean(stack, name, (Boolean) value);
            break;
        }
    }

    public static void writeObjectToCompound(NBTTagCompound compound, Object value, int datatype, String name){
        switch (datatype){
        case FuckingFields.BYTE_ID:
            compound.setByte(name, (Byte) value);
            break;
        case FuckingFields.SHORT_ID:
            compound.setShort(name, (Short) value);
            break;
        case FuckingFields.INT_ID:
            compound.setInteger(name, (Integer) value);
            break;
        case FuckingFields.LONG_ID:
            compound.setLong(name, (Long) value);
            break;
        case FuckingFields.FLOAT_ID:
            compound.setFloat(name, (Float) value);
            break;
        case FuckingFields.DOUBLE_ID:
            compound.setDouble(name, (Double) value);
            break;
        //			case References.CHAR_ID:
        //				ItemNBTHelper.setChar(stack, value.name, (Byte)value.value);
        //				break;
        case FuckingFields.STRING_ID:
            compound.setString(name, (String) value);
            break;
        case FuckingFields.BOOLEAN_ID:
            compound.setBoolean(name, (Boolean) value);
            break;
        }
    }

    public static Object readObjectFromItem(ItemStack stack, int dataType, String name, Object defaultExpected){
        switch (dataType){
        case FuckingFields.BYTE_ID:
            return FuckingNbtHelper.getByte(stack, name, (Byte)defaultExpected);
        case FuckingFields.SHORT_ID:
            return FuckingNbtHelper.getShort(stack, name, (Short)defaultExpected);
        case FuckingFields.INT_ID:
            return FuckingNbtHelper.getInteger(stack, name, (Integer)defaultExpected);
        case FuckingFields.LONG_ID:
            return FuckingNbtHelper.getLong(stack, name, (Long)defaultExpected);
        case FuckingFields.FLOAT_ID:
            return FuckingNbtHelper.getFloat(stack, name, (Float)defaultExpected);
        case FuckingFields.DOUBLE_ID:
            return FuckingNbtHelper.getDouble(stack, name, (Double)defaultExpected);
        //case References.CHAR_ID:
        case FuckingFields.STRING_ID:
            return FuckingNbtHelper.getString(stack, name, (String)defaultExpected);
        case FuckingFields.BOOLEAN_ID:
            return FuckingNbtHelper.getBoolean(stack, name, (Boolean)defaultExpected);
        }
        return null;
    }

    public static Object readObjectFromItem(ItemStack stack, int dataType, String name){
        switch (dataType){
        case FuckingFields.BYTE_ID:
            return FuckingNbtHelper.getByte(stack, name, (byte)0);
        case FuckingFields.SHORT_ID:
            return FuckingNbtHelper.getShort(stack, name, (short) 0);
        case FuckingFields.INT_ID:
            return FuckingNbtHelper.getInteger(stack, name, 0);
        case FuckingFields.LONG_ID:
            return FuckingNbtHelper.getLong(stack, name, 0L);
        case FuckingFields.FLOAT_ID:
            return FuckingNbtHelper.getFloat(stack, name, 0F);
        case FuckingFields.DOUBLE_ID:
            return FuckingNbtHelper.getDouble(stack, name, 0D);
        //case References.CHAR_ID:
        case FuckingFields.STRING_ID:
            return FuckingNbtHelper.getString(stack, name, "");
        case FuckingFields.BOOLEAN_ID:
            return FuckingNbtHelper.getBoolean(stack, name, false);
        }
        return null;
    }

    public static Object readObjectFromCompound(NBTTagCompound compound, int dataType, String name, Object defaultExpected){
        switch (dataType){
        case FuckingFields.BYTE_ID:
            return compound.hasKey(name) ? compound.getByte(name) : (Byte)defaultExpected;
        case FuckingFields.SHORT_ID:
            return compound.hasKey(name) ? compound.getShort(name) : (Short)defaultExpected;
        case FuckingFields.INT_ID:
            return compound.hasKey(name) ? compound.getInteger(name) : (Integer)defaultExpected;
        case FuckingFields.LONG_ID:
            return compound.hasKey(name) ? compound.getLong(name) : (Long)defaultExpected;
        case FuckingFields.FLOAT_ID:
            return compound.hasKey(name) ? compound.getFloat(name) : (Float)defaultExpected;
        case FuckingFields.DOUBLE_ID:
            return compound.hasKey(name) ? compound.getDouble(name) : (Double)defaultExpected;
        //case References.CHAR_ID:
        case FuckingFields.STRING_ID:
            return compound.hasKey(name) ? compound.getString(name) : (String)defaultExpected;
        case FuckingFields.BOOLEAN_ID:
            return compound.hasKey(name) ? compound.getBoolean(name) : (Boolean)defaultExpected;
        }
        return null;
    }

    public static class IntPair
    {
        public int i1;
        public int i2;

        public IntPair(int i1, int i2)
        {
            this.i1 = i1;
            this.i2 = i2;
        }
    }

    public static class XZPair<X, Z>{
        public X x;
        public Z z;

        public XZPair(X x, Z z){
            this.x = x;
            this.z = z;
        }

        public X getKey() { return x; }

        public Z getValue() { return z; }
    }

    public static class XYZTri<X, Y, Z>{
        public X x;
        public Y y;
        public Z z;

        public XYZTri(X x, Y y, Z z){
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }

    public static String formatFileSize(long size) {
        if(size <= 0) return "0";
        final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }
}
