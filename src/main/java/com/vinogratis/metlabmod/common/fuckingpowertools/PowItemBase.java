package com.vinogratis.metlabmod.common.fuckingpowertools;

import com.MetLabMod;
import com.vinogratis.metlabmod.client.helper.ItemNBTHelper;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;

public class PowItemBase extends Item implements IEnergyContainerItem, IConfigurableItem
{
    protected int capacity = 100;
    /**
     * Max Receive
     */
    protected int maxReceive = 1000;
    /**
     * Max Extract
     */
    protected int maxExtract = 100;

    public PowItemBase() {
        this.setMaxStackSize(1);
        this.setCreativeTab(MetLabMod.metLabTabs);
    }

    @Override
    public boolean hasCustomEntity(ItemStack stack) {
        return true;
    }

    /* Energy */

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public void setMaxReceive(int maxReceive) {
        this.maxReceive = maxReceive;
    }

    public void setMaxExtract(int maxExtract) {
        this.maxExtract = maxExtract;
    }

    public int getCapacity(ItemStack stack) {
        return capacity;
    }

    public int getMaxExtract(ItemStack stack) {
        return maxExtract;
    }

    public int getMaxReceive(ItemStack stack) {
        return maxReceive;
    }

    @Override
    public int receiveEnergy(ItemStack container, int maxReceive, boolean simulate) {
        int energy = FuckingNbtHelper.getInteger(container, "Energy", 0);
        int energyReceived = Math.min(getCapacity(container) - energy, Math.min(getMaxReceive(container), maxReceive));

        if (!simulate) {
            energy += energyReceived;
            FuckingNbtHelper.setInteger(container, "Energy", energy);
        }
        return energyReceived;
    }

    @Override
    public int extractEnergy(ItemStack container, int maxExtract, boolean simulate) {
        int energy = FuckingNbtHelper.getInteger(container, "Energy", 0);
        int energyExtracted = Math.min(energy, Math.min(getMaxExtract(container), maxExtract));

        if (!simulate) {
            energy -= energyExtracted;
            FuckingNbtHelper.setInteger(container, "Energy", energy);
        }
        return energyExtracted;
    }

    @Override
    public int getEnergyStored(ItemStack container) {
        return FuckingNbtHelper.getInteger(container, "Energy", 0);
    }

    @Override
    public int getMaxEnergyStored(ItemStack container) {
        return getCapacity(container);
    }

    @Override
    public boolean showDurabilityBar(ItemStack stack) {
        return !(getEnergyStored(stack) == getMaxEnergyStored(stack));
    }

    @Override
    public double getDurabilityForDisplay(ItemStack stack) {
        return 1D - ((double) getEnergyStored(stack) / (double) getMaxEnergyStored(stack));
    }

//    @SideOnly(Side.CLIENT)
//    @SuppressWarnings("unchecked")
//    @Override
//    public void getSubItems(Item item, CreativeTabs tab, List list) {
//        list.add(ItemNBTHelper.setInteger(new ItemStack(item, 1, 0), "Energy", 0));
//        if (capacity > 0) list.add(ItemNBTHelper.setInteger(new ItemStack(item, 1, 0), "Energy", capacity));
//    }

    @Override
    public boolean getHasSubtypes() {
        return capacity > 0;
    }

    /* IConfigurableItem */

    public List<ItemConfigField> getFields(ItemStack stack, int slot) {
        return new ArrayList<ItemConfigField>();
    }

    public List<String> getDisplayData(ItemStack stack) {
        List<String> list = new ArrayList<String>();
        if (hasProfiles()) {
            int preset = FuckingNbtHelper.getInteger(stack, "ConfigProfile", 0);
            list.add(EnumChatFormatting.DARK_PURPLE + StatCollector.translateToLocal("info.de.capacitorMode.txt") + ": " + ItemNBTHelper.getString(stack, "ProfileName" + preset, "Profile " + preset));
        }
        return list;
    }

    public boolean hasProfiles() {
        return true;
    }

}
