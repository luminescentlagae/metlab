package com.vinogratis.metlabmod.common.fuckingpowertools;

import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import net.minecraft.util.StringUtils;

public class ItemConfigField
{

    public Object value;
    public int slot;
    public int datatype;
    public String name;
    public int fieldid;
    public Object max;
    public Object min;
    public Object incroment;
    public String modifier;


    public ItemConfigField(int datatype, int slot, String name) {
        this.slot = slot;
        this.datatype = datatype;
        this.name = name;
    }

    public ItemConfigField(int datatype, Object value, int slot, String name) {
        this.value = value;
        this.slot = slot;
        this.datatype = datatype;
        this.name = name;
    }

    public ItemConfigField setMinMaxAndIncromente(Object min, Object max, Object incroment) {
        this.max = max;
        this.min = min;
        this.incroment = incroment;
        return this;
    }

//    public String getLocalizedName() {
//        return StatCollector.translateToLocal("button.de." + name + ".name");
//    }

    //	public void writeToItem(ItemStack stack){
    //		DataUtills.writeObjectToItem(stack, value, datatype, name);
    //	}

    public ItemConfigField readFromItem(ItemStack stack, Object defaultExpected) {
        value = DataSukaUtils.readObjectFromCompound(IConfigurableItem.ProfileHelper.getProfileCompound(stack), datatype, name, defaultExpected);
        return this;
    }

    public ItemConfigField setModifier(String modifier) {
        this.modifier = modifier;
        return this;
    }

    public String getFormattedValue() {
        if (datatype == FuckingFields.INT_ID && !StringUtils.isNullOrEmpty(modifier) && modifier.equals("MYA")) {
            int i = (Integer) value;
            i *= 2;
            return String.valueOf((i + 1) + "x" + (i + 1));
        } else if (datatype == FuckingFields.BOOLEAN_ID) {
            return (Boolean) value ? StatCollector.translateToLocal("gui.de.on.txt") : StatCollector.translateToLocal("gui.de.off.txt");
        } else if (datatype == FuckingFields.FLOAT_ID && !StringUtils.isNullOrEmpty(modifier) && modifier.equals("PERCENT")) {
            return Math.round((Float) value * 100D) + "%";
        } else if (datatype == FuckingFields.FLOAT_ID && !StringUtils.isNullOrEmpty(modifier) && modifier.equals("PLUSPERCENT")) {
            return "+" + Math.round((Float) value * 100D) + "%";
        } else {
            return String.valueOf(value);
        }
    }

    public String getMaxFormattedValue() {
        if (datatype == FuckingFields.INT_ID && !StringUtils.isNullOrEmpty(modifier) && modifier.equals("MYA")) {
            int i = (Integer) max;
            i *= 2;
            return String.valueOf((i + 1) + "x" + (i + 1));
        } else {
            return String.valueOf(max);
        }
    }


    public int castToInt() {
        switch (datatype) {
        case FuckingFields.BYTE_ID:
            return (int) (Byte) value;
        case FuckingFields.SHORT_ID:
            return (int) (Short) value;
        case FuckingFields.INT_ID:
            return (Integer) value;
        case FuckingFields.LONG_ID:
            long l = (Long) value;
            return (int) l;
        case FuckingFields.FLOAT_ID:
            float f = (Float) value;
            return (int) f;
        case FuckingFields.DOUBLE_ID:
            double d = (Double) value;
            return (int) d;
        case FuckingFields.BOOLEAN_ID:
            return (Boolean) value ? 1 : 0;
        }
        return 0;
    }

    public double castToDouble() {
        switch (datatype) {
        case FuckingFields.BYTE_ID:
            return (double) (Byte) value;
        case FuckingFields.SHORT_ID:
            return (double) (Short) value;
        case FuckingFields.INT_ID:
            return (double) (Integer) value;
        case FuckingFields.LONG_ID:
            long l = (Long) value;
            return (double) l;
        case FuckingFields.FLOAT_ID:
            float f = (Float) value;
            return (double) f;
        case FuckingFields.DOUBLE_ID:
            return (Double) value;
        case FuckingFields.BOOLEAN_ID:
            return (Boolean) value ? 1D : 0D;
        }
        return 0D;
    }

    public double castMinToDouble() {
        switch (datatype) {
        case FuckingFields.BYTE_ID:
            return (double) (Byte) min;
        case FuckingFields.SHORT_ID:
            return (double) (Short) min;
        case FuckingFields.INT_ID:
            return (double) (Integer) min;
        case FuckingFields.LONG_ID:
            long l = (Long) min;
            return (double) l;
        case FuckingFields.FLOAT_ID:
            float f = (Float) min;
            return (double) f;
        case FuckingFields.DOUBLE_ID:
            return (Double) min;
        case FuckingFields.BOOLEAN_ID:
            return 0D;
        }
        return 0D;
    }

    public double castMaxToDouble() {
        switch (datatype) {
        case FuckingFields.BYTE_ID:
            return (double) (Byte) max;
        case FuckingFields.SHORT_ID:
            return (double) (Short) max;
        case FuckingFields.INT_ID:
            return (double) (Integer) max;
        case FuckingFields.LONG_ID:
            long l = (Long) max;
            return (double) l;
        case FuckingFields.FLOAT_ID:
            float f = (Float) max;
            return (double) f;
        case FuckingFields.DOUBLE_ID:
            return (Double) max;
        case FuckingFields.BOOLEAN_ID:
            return 1D;
        }
        return 0D;
    }
}
