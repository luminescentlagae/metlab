package com.vinogratis.metlabmod.common.fuckingpowertools;

import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import org.lwjgl.input.Keyboard;

import java.util.List;

public class FuckingInfoSukaHelper
{
    @SuppressWarnings("unchecked")
    public static void addEnergyInfo(ItemStack stack, List list)
    {

        IEnergyContainerItem item = (IEnergyContainerItem) stack.getItem();
        int energy = item.getEnergyStored(stack);
        int maxEnergy = item.getMaxEnergyStored(stack);
        String eS = "";
        String eM = "";
        if (energy < 1000)
            eS = String.valueOf(energy);
        else if (energy < 1000000)
            eS = String.valueOf(energy);
        else
            eS = String.valueOf(Math.round((float) energy / 1000F) / 1000F) + "m";
        if (maxEnergy < 1000)
            eM = String.valueOf(maxEnergy);
        else if (maxEnergy < 1000000)
            eM = String.valueOf(Math.round((float) maxEnergy / 100F) / 10F) + "k";
        else
            eM = String.valueOf(Math.round((float) maxEnergy / 10000F) / 100F) + "m";

        list.add(StatCollector.translateToLocal("mya") + ": " + eS + " / " + eM + " myarf");
    }

    public static boolean isShiftKeyDown()
    {
        return Keyboard.isKeyDown(Keyboard.KEY_LSHIFT) || Keyboard.isKeyDown(Keyboard.KEY_RSHIFT);
    }

    public static boolean isCtrlKeyDown()
    {
        return Keyboard.isKeyDown(Keyboard.KEY_LCONTROL) || Keyboard.isKeyDown(Keyboard.KEY_RCONTROL);
    }
}