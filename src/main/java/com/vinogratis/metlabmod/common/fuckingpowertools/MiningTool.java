package com.vinogratis.metlabmod.common.fuckingpowertools;

import com.vinogratis.metlabmod.common.items.poweritems.HuinyaTool;
import net.minecraft.block.Block;
import net.minecraft.block.material.MaterialLiquid;
import net.minecraft.client.Minecraft;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.server.S23PacketBlockChange;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.event.world.BlockEvent;

import java.util.HashMap;
import java.util.Map;

public class MiningTool extends HuinyaTool
{

        public MiningTool(ToolMaterial material) {
            super(0, material, null);
        }

        public Map<Block, Integer> getObliterationList(ItemStack stack) {
            Map<Block, Integer> blockMap = new HashMap<Block, Integer>();

            NBTTagCompound compound = FuckingNbtHelper.getCompound(stack);

            if (compound.hasNoTags()) return blockMap;
            for (int i = 0; i < 9; i++) {
                NBTTagCompound tag = new NBTTagCompound();
                if (compound.hasKey("Item" + i)) tag = compound.getCompoundTag("Item" + i);

                if (tag.hasNoTags()) continue;

                ItemStack stack1 = ItemStack.loadItemStackFromNBT(tag);

                if (stack1 != null && stack1.getItem() instanceof ItemBlock)
                    blockMap.put(Block.getBlockFromItem(stack1.getItem()), stack1.getItemDamage());
            }

            return blockMap;
        }


        protected void breakExtraBlock(ItemStack stack, World world, int x, int y, int z, int totalSize, EntityPlayer player, float refStrength, boolean breakSound, Map<Block, Integer> blockMap) {
            if (world.isAirBlock(x, y, z)) return;

            Block block = world.getBlock(x, y, z);
            if (block.getMaterial() instanceof MaterialLiquid || (block.getBlockHardness(world, x, y, x) == -1 && !player.capabilities.isCreativeMode))
                return;

            int meta = world.getBlockMetadata(x, y, z);

            boolean effective = false;

            for (String s : getToolClasses(stack)) {
                if (block.isToolEffective(s, meta) || func_150893_a(stack, block) > 1F) effective = true;
            }

            if (!effective) return;

            float strength = ForgeHooks.blockStrength(block, player, world, x, y, z);

            if (!player.canHarvestBlock(block) || !ForgeHooks.canHarvestBlock(block, player, meta) || refStrength / strength > 10f && !player.capabilities.isCreativeMode)
                return;

            if (!world.isRemote) {
                BlockEvent.BreakEvent event = ForgeHooks.onBlockBreakEvent(world, world.getWorldInfo().getGameType(), (EntityPlayerMP) player, x, y, z);
                if (event.isCanceled()) {
                    ((EntityPlayerMP) player).playerNetServerHandler.sendPacket(new S23PacketBlockChange(x, y, z, world));
                    return;
                }
            }

            int scaledPower = energyPerOperation + (totalSize * (energyPerOperation / 10));

            if (player.capabilities.isCreativeMode || (blockMap.containsKey(block) && blockMap.get(block) == meta)) {

                block.onBlockHarvested(world, x, y, z, meta, player);
                if (block.removedByPlayer(world, player, x, y, z, false))
                    block.onBlockDestroyedByPlayer(world, x, y, z, meta);

                if (!world.isRemote) {
                    ((EntityPlayerMP) player).playerNetServerHandler.sendPacket(new S23PacketBlockChange(x, y, z, world));
                }

                if ((blockMap.containsKey(block) && blockMap.get(block) == meta)) extractEnergy(stack, scaledPower, false);
                if (breakSound) world.playAuxSFX(2001, x, y, z, Block.getIdFromBlock(block) + (meta << 12));
                return;
            }

            extractEnergy(stack, scaledPower, false);

            if (!world.isRemote) {

                block.onBlockHarvested(world, x, y, z, meta, player);

                if (block.removedByPlayer(world, player, x, y, z, true)) {
                    block.onBlockDestroyedByPlayer(world, x, y, z, meta);
                    block.harvestBlock(world, player, x, y, z, meta);
                    player.addExhaustion(-0.025F);
                    if (block.getExpDrop(world, meta, EnchantmentHelper.getFortuneModifier(player)) > 0)
                        player.addExperience(block.getExpDrop(world, meta, EnchantmentHelper.getFortuneModifier(player)));
                }

                EntityPlayerMP mpPlayer = (EntityPlayerMP) player;
                mpPlayer.playerNetServerHandler.sendPacket(new S23PacketBlockChange(x, y, z, world));
            } else {
                if (breakSound) world.playAuxSFX(2001, x, y, z, Block.getIdFromBlock(block) + (meta << 12));
                if (block.removedByPlayer(world, player, x, y, z, true)) {
                    block.onBlockDestroyedByPlayer(world, x, y, z, meta);
                }

                Minecraft.getMinecraft().getNetHandler().addToSendQueue(new C07PacketPlayerDigging(2, x, y, z, Minecraft.getMinecraft().objectMouseOver.sideHit));
            }
        }



}