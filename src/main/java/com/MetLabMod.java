package com;

import com.vinogratis.metlabmod.client.chunkloader.utils.BlockMyaGenerator;
import com.vinogratis.metlabmod.client.helper.GuiHandler;
import com.vinogratis.metlabmod.client.helper.LibMisc;
import com.vinogratis.metlabmod.common.blocks.BlockDegenerator;
import com.vinogratis.metlabmod.common.blocks.BlockGenerator;
import com.vinogratis.metlabmod.common.blocks.BlockKiller;
import com.vinogratis.metlabmod.common.blocks.BlockLevitator;
import com.vinogratis.metlabmod.common.blocks.BlockMagnet;
import com.vinogratis.metlabmod.common.blocks.BlockMobBarrier;
import com.vinogratis.metlabmod.common.blocks.BlockNormalGenerator;
import com.vinogratis.metlabmod.common.blocks.BlockOculus;
import com.vinogratis.metlabmod.common.blocks.BlockQuarry;
import com.vinogratis.metlabmod.common.blocks.BlockRepairer;
import com.vinogratis.metlabmod.common.blocks.BlockStorage;
import com.vinogratis.metlabmod.common.blocks.MetBlock;
import com.vinogratis.metlabmod.common.items.ItemOculus;
import com.vinogratis.metlabmod.common.items.MetChest;
import com.vinogratis.metlabmod.common.items.MetItem;
import com.vinogratis.metlabmod.common.tileentities.TileDegenerator;
import com.vinogratis.metlabmod.common.tileentities.TileGenerator;
import com.vinogratis.metlabmod.common.tileentities.TileKiller;
import com.vinogratis.metlabmod.common.tileentities.TileLevitator;
import com.vinogratis.metlabmod.common.tileentities.TileMagnet;
import com.vinogratis.metlabmod.common.tileentities.TileMobBarrier;
import com.vinogratis.metlabmod.common.tileentities.TileNormalGenerator;
import com.vinogratis.metlabmod.common.tileentities.TileQuarry;
import com.vinogratis.metlabmod.common.tileentities.TileRepairer;
import com.vinogratis.metlabmod.common.tileentities.TileStorage;
import com.vinogratis.metlabmod.config.MetLabBlocks;
import com.vinogratis.metlabmod.config.MetLabItems;
import com.vinogratis.metlabmod.config.MetLabTileEntities;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

@Mod(modid = MetLabMod.MODID, version = MetLabMod.VERSION)
public class MetLabMod
{
    public static final String MODID = "metlabmod";
    public static final String VERSION = "1.0";

    public static final CreativeTabs metLabTabs = new CreativeTabs("metLabTabs")
    {
        @Override
        public Item getTabIconItem()
        {
            return Item.getItemFromBlock(Blocks.leaves);
        }
    };

    @Instance(LibMisc.MOD_ID)
    public static MetLabMod instance =  new MetLabMod();


    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        NetworkRegistry.INSTANCE.registerGuiHandler(MetLabMod.instance, new GuiHandler());
    }

    @EventHandler
    public void preLoad(FMLPreInitializationEvent event)
    {
        MetLabBlocks.preInit();
        MetLabItems.preInit();
        MetLabTileEntities.preInit();
        BlockMyaGenerator blockmyagenerator = new BlockMyaGenerator();
        GameRegistry.registerWorldGenerator(blockmyagenerator, 0);

        GameRegistry.addRecipe(new ItemStack(MetLabBlocks.reactor, 1),
                "###",
                "XYX",
                "###",
                ('X'), Blocks.iron_block, ('#'), Items.iron_ingot, ('Y'), Blocks.obsidian);
    }
}
